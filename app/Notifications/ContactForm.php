<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactForm extends Notification implements ShouldQueue
{
    use Queueable;

    private $object;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('New contact information (No: ' . $this->object->id . ')')
                ->line('Hi, ' . $notifiable->name)
                ->line('You have new contact from,')
                ->line('Full Name: ' . $this->object->name)
                ->line('Email: ' . $this->object->getProperty('email'))
                // ->line('IP: ' . $_SERVER['REMOTE_ADDR'])
                ->action('Notification Action', url('/'))
                ->line('Please check in web master for more info.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
