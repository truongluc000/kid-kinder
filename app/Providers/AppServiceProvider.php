<?php
 
namespace App\Providers;

use App\Models\Menu;
use App\Models\ConfigGeneral;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        $configItem = ConfigGeneral::find(config('constants.defaultIdConfig'));
        if(!empty($configItem)){
            $configData = unserialize($configItem->properties);
            isset($configData['version_home'])?$configData['version_home']:"1";
            View::share('configData', $configData);#share config data to all view, ex: logo, page's name from config
        }
        
        $homeMenus = Menu::select('*')->where('status','1')->get();
        if($homeMenus){
            View::share('homeMenus', $homeMenus);#share home menu for all page
        }
        # Paginator::defaultView('vendor.pagination.bootstrap');// set pagination for all pages
    }
}