<?php
    
namespace App;

class Constants {
    
    public static $status = [
        '0' => 'Đã xóa',
        '1' => 'Hoạt động',
        '2' => 'Vô hiệu',
    ];
}