<?php

namespace App\Http\Controllers\Api;

use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    public function index(){
        $teacherList = Teacher::select('*')->orderBy("created_at", "DESC")->get();
        return $teacherList;
    } 
    public function store(Request $request)
    {
            $properties = array();
            $properties['twitter'] = $request->twitter?$request->twitter:"";
            $properties['facebook'] = $request->facebook?$request->facebook:"";
            $properties['linkedin'] = $request->linkedin?$request->linkedin:"";
            $properties['avatar'] = $request->avatar?$request->avatar:"";
            $properties['sapo'] = $request->sapo?$request->sapo:"";
            $dataTeacher = array();
            $dataTeacher['user_id']= $request->user_id?$request->user_id:1;
            $dataTeacher['name']= $request->name?$request->name:"";
            $dataTeacher['position']= $request->position?$request->position:0;
            $dataTeacher['properties']= serialize($properties);
            $dataTeacher['status']= $request->status?$request->status:0;
            $item = Teacher::create($dataTeacher);
            if($item){
                return array("error" => 0, "msg" => "success");
            }
            return array("error" => 1, "msg" => "failed");
    }
}
