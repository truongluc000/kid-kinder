<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class LibraryController extends Controller
{
    public function index(){
        try {
            return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    } 
}
