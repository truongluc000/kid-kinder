<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use App\Models\Ads;
use App\Models\User;
use App\Models\Article;
use App\Models\Classes;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Statics;
use App\Models\Teacher;
use App\Models\Comments;
use Illuminate\Http\Request;
use App\Models\AdsCategories;
use App\Models\ConfigGeneral;
use App\Services\StaticService;
use App\Services\ArticleService;
use App\Models\ArticleCategories;
use App\Notifications\ContactForm;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\CommentRequest;
use Illuminate\Auth\Events\Validated;
use App\Services\ArticleCategoryService;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\StaticNotFoundException;
use App\Exceptions\ArticleNotFoundException;
use Illuminate\Support\Facades\Notification;
use App\Exceptions\ArticleCategoryNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\RelationNotFoundException;

class HomeController extends Controller
 
{ 
    public function index(){ 
        $adsList = Ads::with('categories')->get();
        // $adsCateList = AdsCategories::with('listItem')->get();
        $aboutItem = Statics::where('slug', '=', 'about-us')->where('status',1)->first();// can use firstOrFail() to return 404 if not found object
        $classesItem = Statics::where('slug', '=', 'our-class')->where('status',1)->first();
        $classesList = Classes::select('*')->where('status',1)->get();
        $teacherList = Teacher::select('*')->where('status',1)->limit(4)->get();
        $articleList = Article::with(['categories','author','listComments'])->orderBy("created_at", "DESC")->limit(3)->get();
        
        return view('pages.index', [
            'objectItem' => array(),
            'adsList' => $adsList,
            'aboutItem' => $aboutItem,
            'classesItem' => $classesItem,
            'classesList' => $classesList, 
            'teacherList' => $teacherList,
            'articleList' => $articleList,
        ]); 
        //also can use: compact('adsList','adsCateList','aboutItem')
    } 
    public function page($slug){
        //based on the slug that defines the controller to get data
        switch ($slug) {
            case 'about-us':
                #get data about
                try{
                    $aboutItem = Statics::where('slug', '=', $slug)->where('status',1)->firstOrFail();//or firstOrFail | run exception if not found
                    $adsList = Ads::with('categories')->where('status',1)->get();
                    $teacherList = Teacher::select('*')->where('status',1)->limit(4)->get();
                    // $aboutItem->load(['saaa']); // try to load catch RelationNotFoundException
                } catch(RelationNotFoundException $exception){
                    return view('errors.relations'); // return relations page if not found RelationNot of aboutItem data in try
                } catch(ModelNotFoundException $exception){
                    // dd($exception->getMessage());// return error message 
                    // dd(get_class($exception));// return exception class
                    return view('errors.models'); // return models page if not found Model in try
                } catch(Exception $exception){
                    return view('errors.static_notfound'); // return about_notfound page if get any Exception in try
                }
                return view('pages.about',[
                    'objectItem' => $aboutItem,
                    'aboutItem' => $aboutItem,
                    'adsList' => $adsList,
                    'teacherList' => $teacherList,
                ]);// anything good, return data
                break;
            case 'our-class':  
                #get data ourclass
                try {
                    $classesItem = (new StaticService())->findBySlug($slug);
                    $classesList = Classes::select('*')->get();
                } catch (StaticNotFoundException $exception){
                    return view('errors.static_notfound',['error' => $exception->getMessage()]);
                }
                return view('pages.ourclass',[
                    'objectItem' => $classesItem,
                    'classesItem' => $classesItem,
                    'classesList' => $classesList,
                ]);
                break;
            case 'teachers':
                #get data teachers
                try {
                    $teacherItem = (new StaticService())->findBySlug($slug);
                    $teacherList = Teacher::select('*')->where('status',1)->get();
                    $adsList = Ads::with('categories')->get();
                } catch (StaticNotFoundException $exception){
                    return view('errors.static_notfound',['error' => $exception->getMessage()]);
                }
                return view('pages.teachers',[
                    'objectItem' => $teacherItem,
                    'teacherItem' => $teacherItem,
                    'teacherList' => $teacherList,
                    'adsList' => $adsList,
                ]);
                break;
            case 'gallery':
                #get data gallery
                try {
                    $galleryItem = (new StaticService())->findBySlug($slug);
                    $listGallery = Gallery::select('*')->where('status',1)->get();
                } catch (StaticNotFoundException $exception){
                    return view('errors.static_notfound',['error' => $exception->getMessage()]);
                }
                return view('pages.gallery',[
                    'objectItem' => $galleryItem,
                    'galleryItem' => $galleryItem,
                    'listGallery' => $listGallery,
                ]);
                break;
            case 'blogs':
                #get data blogs
                try {
                    $blogItem = (new StaticService())->findBySlug($slug);
                    $listBlogs = Article::with(['categories','author','listComments'])->where('status',1)->orderBy("created_at", "DESC")->paginate(config('constants.articleHomePerPage'));
                } catch (StaticNotFoundException $exception){
                    return view('errors.static_notfound',['error' => $exception->getMessage()]);
                }
                return view('pages.blogs',[
                    'objectItem' => $blogItem,
                    'blogItem' => $blogItem,
                    'list' => $listBlogs,
                ]);
                break;
            case 'contact':
                #get data contact
                try {
                    $contactItem = (new StaticService())->findBySlug($slug);
                    $listBlogs = Article::with(['categories','author','listComments'])->where('status',1)->orderBy("created_at", "DESC")->paginate(config('constants.articleHomePerPage'));
                } catch (StaticNotFoundException $exception){
                    return view('errors.static_notfound',['error' => $exception->getMessage()]);
                }
                return view('pages.contact',[
                    'objectItem' => $contactItem,
                    'contactItem' => $contactItem,
                    'list' => $listBlogs, 
                ]); 
                break;
            default:
                #if slug ia an article category => return blog list
                try {
                    $categoryItem = (new ArticleCategoryService())->findBySlug($slug);
                    $listBlogs = Article::with(['categories','author','listComments'])->where('pid',$categoryItem->id)->where('status',1)->orderBy("created_at", "DESC")->paginate(config('constants.articleHomePerPage'));
                    return view('pages.blogs',[
                        'objectItem' => $categoryItem,
                        'blogItem' => $categoryItem,
                        'list' => $listBlogs,
                    ]);
                } catch (ArticleCategoryNotFoundException $exception){
                    abort(404);// return 404 if nothing found
                }
                break;
                
        }
    } 
    public function article($category, $slug){
        try { 
            $categoryItem = (new ArticleCategoryService())->findBySlug($category);
            $articleItem = (new ArticleService())->findBySlug($slug);
            $otherPost = Article::with(['categories','author','listComments'])->where('id','<>',$articleItem->id)->where('pid',$articleItem->pid)->where('status',1)->orderBy("created_at", "DESC")->limit(3)->get();
            $listCategory = ArticleCategories::with('listItem')->where('status',1)->orderBy("name", "ASC")->get();
            $listComments = Comments::with('article')->where('status',1)->where('article_id',$articleItem->id)->orderBy("id", "DESC")->get();
        }catch (ArticleCategoryNotFoundException $exception){
            return view('errors.article_category_notfound',['error' => $exception->getMessage()]);
        } catch (ArticleNotFoundException $exception){
            return view('errors.article_notfound',['error' => $exception->getMessage()]);
        }
        return view('pages.blogdetail',[
            'objectItem' => $articleItem,
            'categoryItem' => $categoryItem,
            'otherPost' => $otherPost,
            'listCategory' => $listCategory,
            'listComments' => $listComments,
        ]);
    }
    public function saveComment(Request $request){
        $rules = [
            'name' => 'required|min:3|max:20',
            'email' => 'required|email',
            'message' => 'required|min:20|max:800'
        ];
        $input = [
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message
        ];
        $validator = Validator::make($input, $rules); 
        if($validator->fails()){// if fail
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => $validator->errors()->toArray(),'msg' => __('messages.invalid_data')]);
        }else{
            //add data
            $properties['message'] = $request->message;
            $properties['email'] = $request->email;
            $articleItem = (new ArticleService())->findBySlug($request->article_slug);
            if($articleItem){
                $item = Comments::create([
                    'pid' => $request->pid?$request->pid:0,
                    'article_id' => $articleItem->id,
                    'name' => $request->name,
                    'properties' => serialize($properties),
                    'status' => 0,// default status is 0, this status only can active in web admin
                    'created_at' => Carbon::now()
                ]);
                if($item){
                    return response()->json(['status' => '1', 'icon' => 'success', 'msg' => __('messages.send_comment_success')]);
                }else{
                    return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.an_error_occurred')]);
                }
            }else{
                return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.an_error_occurred')]);
            }
        }
    }
    public function saveContact(Request $request){
        $rules = [
            'name' => 'required|min:3|max:20',
            'email' => 'required|email',
            'message' => 'required|min:20|max:800',
            'subject' => 'required|min:10|max:200'
        ];
        $input = [
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
            'subject' => $request->subject
        ];
        $validator = Validator::make($input, $rules); 
        if($validator->fails()){// if fail
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => $validator->errors()->toArray(),'msg' => __('messages.invalid_data')]);
        }else{
            //add data
            $properties['message'] = $request->message;
            $properties['email'] = $request->email;
            $properties['subject'] = $request->subject;
            $item = Contact::create([
                'name' => $request->name,
                'properties' => serialize($properties),
                'status' => 0,// default status is 0, this status only can active in web admin
                'created_at' => Carbon::now()
            ]);
            if($item){
                //send notification to email in config constants
                // $admin = User::where('email','=',config('constants.defaultEmailReceive'))->first();
                // if($admin)$admin->notify(new ContactForm($item));
                //response
                return response()->json(['status' => '1', 'icon' => 'success', 'msg' => __('messages.send_contact_success')]);
            }else{
                return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.an_error_occurred')]);
            }
        }
    }  
    public function saveNewsletter(Request $request){
        $rules = [
            'name' => 'required|min:3|max:20',
            'email' => 'required|email'
        ];
        $input = [
            'name' => $request->name,
            'email' => $request->email
        ];
        $validator = Validator::make($input, $rules); 
        if($validator->fails()){// if fail
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => $validator->errors()->toArray(),'msg' => __('messages.invalid_data')]);
        }else{
            //add data
            $properties['message'] = $request->message;
            $properties['email'] = $request->email;
            $properties['subject'] = $request->subject;
            $item = Contact::create([
                'name' => $request->name,
                'properties' => serialize($properties),
                'status' => 0,// default status is 0, this status only can active in web admin
                'type' => config('constants.newsletter'),
                'created_at' => Carbon::now()
            ]);
            if($item){
                //send notification to email in config constants
                // $admin = User::where('email','=',config('constants.defaultEmailReceive'))->first();
                // if($admin)$admin->notify(new ContactForm($item));
                return response()->json(['status' => '1', 'icon' => 'success', 'msg' => __('messages.send_newsletter_success')]);
            }else{
                return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.an_error_occurred')]);
            }
        }
    }
    public function bookASeat(Request $request){
        $rules = [
            'name' => 'required|min:3|max:20',
            'email' => 'required|email',
            'classes' => 'required'
        ];
        $input = [
            'name' => $request->name,
            'email' => $request->email,
            'classes' => $request->classes
        ];
        $validator = Validator::make($input, $rules); 
        if($validator->fails()){// if fail
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => $validator->errors()->toArray(),'msg' => __('messages.invalid_data')]);
        }else if(!isset($request->classes) || $request->classes == '0'){
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => array(),'msg' => __('messages.select_class_required')]);
        }else{
            //add data
            $properties['email'] = $request->email;
            $item = Contact::create([
                'name' => $request->name,
                'properties' => serialize($properties),
                'status' => 0,// default status is 0, this status only can active in web admin
                'type' => config('constants.bookASeat'),
                'classes' => $request->classes,
                'created_at' => Carbon::now()
            ]);
            if($item){
                //send notification to email in config constants
                // $admin = User::where('email','=',config('constants.defaultEmailReceive'))->first();
                // if($admin)$admin->notify(new ContactForm($item));
                return response()->json(['status' => '1', 'icon' => 'success', 'msg' => __('messages.book_a_seat_success')]);
            }else{
                return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.an_error_occurred')]);
            }
        }
    }
    public function novel(){ 
        // get api data from truyennovel.com
        $novelResponse = json_decode(Http::get('https://truyennovel.com/api2022.php',[
            'op' => env('TRUYENNOVEL_API_OP'),
            'act' => env('TRUYENNOVEL_API_ACT'),
            'ver' => env('TRUYENNOVEL_API_VER'),
            'app_key' => env('TRUYENNOVEL_API_KEY'),
            'store_id' => env('TRUYENNOVEL_API_STORE_ID'),
            'plus' => 'list'
        ]), true);
        $novelList = array();
        if(isset($novelResponse['error']) && $novelResponse['error'] == '0'){
            $novelList = $novelResponse['data'];
            return view('pages.novel', ['novelList' => $novelList]); 
        }else{
            abort(404);
        }
    } 
}
