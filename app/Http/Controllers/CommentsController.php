<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use App\Models\Comments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CommentsController extends Controller
 
{
    public function index(){ 
        try {
            $list = Comments::with('article')->select('*')->orderBy("created_at", "DESC")->paginate(config('constants.itemPerPage'));// total page with condition and item per page. ->where('status','=',1) for condition
            return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2), ['list' => $list]);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    } 
    public function create(){
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_create');// view folder name with 's' . file without 's'
    }  
    public function store(Request $request)
    {
        if ($request->is(config('constants.adminLink').'/'.request()->segment(2).'/*')) {
            $properties = array();
            $properties['message'] = $request->message?$request->message:"";
            $properties['email'] = $request->email?$request->email:"";

            $item = Comments::create([
                'article_id' => $request->article_id?$request->article_id:0,
                'pid' => $request->pid?$request->pid:0,
                'name' => $request->name?$request->name:"",
                'properties' => serialize($properties),
                'status' => $request->status?$request->status:1,
                'created_at' => Carbon::now()
            ]);
            if($item){
                $this->storeTracking(__('messages.add_new_comment_success',['id' => $item->id]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.add_new_success'));
            }
        }
        return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
    } 
    public function show($id){
        try { 
            $item = Comments::find($id);
            if($item){
                $properties = $item->getProperties();
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_show',['item' => $item,'properties' => $properties]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.item_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function edit(Request $request)
    {
        try {
            $item = Comments::find($request->id);
            $properties = array();
            if(!empty($item->properties)){
                $properties = $item->getProperties();
            }
            $properties['message'] = $request->message?$request->message:"";
            $properties['email'] = $request->email?$request->email:"";

            $data = array(
                'article_id' => $request->article_id?$request->article_id:0,
                'pid' => $request->pid?$request->pid:0,
                'name' => $request->name?$request->name:"",
                'properties' => serialize($properties),
                'status' => $request->status,
            );
            $item->update($data);
            $this->storeTracking(__('messages.update_comment_success',['id' => $item->id]));
            return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('success', __('messages.update_success'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function changeStatus(Request $request)
    {
        try {
                $item = Comments::find($request->id);
                $item->update(['status'   => $request->status]);
                $this->storeTracking(__('messages.change_comment_status',['id' => $request->id,'status' => config('constants.status.'.$request->status)]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success_id', ['id' => $request->id]));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function update(Request $request)//update status for checked items
    {
        try {
            $newStatus = $request->new_status?$request->new_status:"0";
            if(!empty($request->ids)){
                foreach ($request->ids as $id) {
                    $item = Comments::find($id);
                    $item->update(['status' => $newStatus]);
                    $this->storeTracking(__('messages.change_comment_status',['id' => $id,'status' => config('constants.status.'.$newStatus)]));
                }
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.no_item_selected'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function clean(){
        //return cleantrash view
        return view('/'.config('constants.adminLink').'/include/clean_trash');
    }
    public function destroy(){
        // delete all items with deleted status
        Comments::select('*')->where('status','=',config('constants.delete'))->delete();
        $this->storeTracking(__('messages.clean_comment_list'));
        return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.clean_trash_success'));
    }
}
