<?php

namespace App\Http\Controllers;
use Exception;
use App\Models\User;
use App\Models\Article;
use App\Models\Classes;
use App\Models\Gallery;
use App\Models\Teacher;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Notifications\RegisterUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Notification;

class AuthController extends Controller
{  
    protected $authService;

    public function index(){
        // check login

        if (Auth::check()) {
            $totalUser = count(User::select('*')->get());
            $totalClasses = count(Classes::select('*')->get());
            $totalTeacher = count(Teacher::select('*')->get());
            $totalGallery = count(Gallery::select('*')->get());
            $totalArticle = count(Article::select('*')->get());
            return view(config('constants.adminLink').'/dashboard', [
                'totalUser' => $totalUser,
                'totalClasses' => $totalClasses,
                'totalTeacher' => $totalTeacher,
                'totalGallery' => $totalGallery,
                'totalArticle' => $totalArticle,
        ]);
        } else {
            return redirect(config('constants.adminLink').'/login'); 
        }
    }

    public function login(){
        // if(Session::has('userInfo')){
        //     return redirect(config('constants.adminLink').'/dashboard');
        // }
        
        if (Auth::check()) {
            return redirect(config('constants.adminLink').'/dashboard');
        } else {
            return view(config('constants.adminLink').'/login');
        }
    }

    public function register(){
        return view(config('constants.adminLink').'/register');
    }

    public function loadingpage(){
        return view(config('constants.adminLink').'/loadingpage');
    }

    public function postLogin(Request $request)
    {
        if ($request->is(config('constants.adminLink').'/*')) {
            $data = [
                'email' => $request->email,
                'password' => $request->password,
                'status' => config('constants.enable'),
            ];
            // Đăng nhập
            if (!Auth::attempt($data)) {
                return redirect(config('constants.adminLink').'/login')->with('error', 'Username / password incorrect or inactive');
            }
            return redirect(config('constants.adminLink').'/')->with('success', __('message.login_success'));
        }
    }

    public function logout(){
        Auth::logout();
        return redirect(config('constants.adminLink').'/login')->with('success', __('message.logout_success'));
    }

    public function postRegister(Request $request)//RegisterRequest
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|same:password'
        ];
        $input = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        ];
        $validator = Validator::make($input, $rules); 
        if($validator->fails()){// if fail
            return response()->json(['status' => '0', 'icon' => 'warning', 'error' => $validator->errors()->toArray(),'msg' => __('messages.invalid_data')]);
        }else{
            // check email_exist
            if (User::where('email', '=', $request->email)->exists()) {
                return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.email_exist')]);
            }else{
                //add data
                $user = User::create([
                    'user_id' => 0,
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'level' => 1,
                    'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/logo/kid-circle.png')),
                    'status' => config('constants.enable'),
                    'created_at' => Carbon::now()
                ])->assignRole('user'); 
                if (!Auth::attempt(['email' => $request->email,'password' => $request->password])) {
                    return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.user_password_incorrect')]);
                }  
                if($user){
                    $user->update(array('api_token' => $user->id.Str::random(59)));// update token to user
                    // send mail
                    $admin = User::where('email','=',config('constants.defaultEmailReceive'))->first();
                    if($admin)$admin->notify(new RegisterUser($user));
                    // Notification::send($admin, new RegisterUser($user));
                    Session::put('userInfo', $user);
                    return response()->json(['status' => '1', 'icon' => 'success', 'msg' => __('messages.register_successfully')]);
                }else{
                    return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.failed_registration')]);
                }
            }
        }
    }
    public function show(){
        $item = Auth::user();
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2).'_show',['item' => $item]);
    }
    public function edit(UserUpdateRequest $request)
    { 
        try {
            $authUser = Auth::user();
            if (User::where('email', '=', $request->email)->where('id', '<>', $authUser->id)->exists()) {
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.email_exist'))->withInput($request->except('password'));
            }else{
                $item = User::find($authUser->id);
                $properties = array();
                if(!empty($item->properties)){
                    $properties = $item->getProperties();
                }
                $properties['avatar'] = $request->avatar?$request->avatar:"";
                $data = array(
                    'name'     => $request->name,
                    'email'    => $request->email,
                    'properties' => serialize($properties)
                ); 
                if(!empty($request->password)){
                    $data['password'] = bcrypt($request->password);
                }
                $item->update($data);
                $this->storeTracking(__('messages.update_info_success'));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function token()//update token
    {
        $authUser = Auth::user();
        $token = $authUser->id.Str::random(59);
        $currentUser = User::find($authUser->id);
        $checkUpdate = $currentUser->update([
            'api_token' => $token,
        ]);
        if($checkUpdate){
            $this->storeTracking(__('messages.update_api_token'));
            return response()->json(['status' => '1', 'icon' => 'success','token' => $token, 'msg' => __('messages.generate_success')]);
        }else{
            return response()->json(['status' => '2', 'icon' => 'error', 'msg' => __('messages.failed')]);
        }
    }
    // public function checkPermission($permissionName = '')
    // {
    //     if(!$permissionName) return false;
    //     if(Auth::user()->hasRole('admin')){
    //         return true;// if admin => allowed to see all
    //     }else if(Auth::user()->hasRole('user') && Auth::user()->hasPermissionTo($permissionName)){// if user => check permission
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }
}
