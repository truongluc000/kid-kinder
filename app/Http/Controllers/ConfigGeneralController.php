<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use App\Models\ConfigGeneral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ConfigGeneralController extends Controller
{
    public function show(){ 
        try {
            
            $item = ConfigGeneral::find(config('constants.defaultIdConfig'));// default id for config is 1
            if($item){
                $properties = $item->getProperties();
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2).'_show',['item' => $item,'properties' => $properties]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.item_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function edit(Request $request)
    {
        try {
            $item = ConfigGeneral::find(config('constants.defaultIdConfig'));// default id for config is 1
            $properties = array();
            if(!empty($item->properties)){
                $properties = $item->getProperties();
            }
            $properties['name'] = $request->name?$request->name:"";
            $properties['keywords'] = $request->keywords?$request->keywords:"";
            $properties['sapo'] = $request->sapo?$request->sapo:"";
            $properties['copyright'] = $request->copyright?$request->copyright:"";
            $properties['email'] = $request->email?$request->email:"";
            $properties['tel'] = $request->tel?$request->tel:"";
            $properties['address'] = $request->address?$request->address:"";
            $properties['twitter'] = $request->twitter?$request->twitter:"";
            $properties['facebook'] = $request->facebook?$request->facebook:"";
            $properties['linkedin'] = $request->linkedin?$request->linkedin:"";
            $properties['instagram'] = $request->instagram?$request->instagram:"";
            $properties['avatar'] = $request->avatar?$request->avatar:"";
            $properties['favicon'] = $request->favicon?$request->favicon:"";
            $properties['version_home'] = $request->version_home?$request->version_home:"";
            $properties['version_admin'] = $request->version_admin?$request->version_admin:"";
            $properties['opening_days'] = $request->opening_days?$request->opening_days:"";
            $properties['opening_hours'] = $request->opening_hours?$request->opening_hours:"";

            $data = array(
                'name' => $request->name,
                'position' => $request->position?$request->position:0,
                'properties' => serialize($properties),
                'status' => config('constants.enable'),
            );
            $item->update($data);
            $this->storeTracking(__('messages.update_config_success'));
            return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
}
