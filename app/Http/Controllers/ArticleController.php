<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use App\Models\Article;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ArticleCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ArticleController extends Controller
 
{
    public function index(){
        try {
            $list = Article::select('*')->orderBy("created_at", "DESC")->paginate(config('constants.itemPerPage'));// total page with condition and item per page. ->where('status','=',1) for condition
            return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2), ['list' => $list]);
        } catch (Exception $exception) { 
            return back()->withError($exception->getMessage())->withInput();
        }
    }  
    public function create(){ 
        $list = ArticleCategories::select('*')->get();
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_create', ['list' => $list]);// view folder name with 's' . file without 's'
    }  
    public function store(Request $request)
    {
        if ($request->is(config('constants.adminLink').'/'.request()->segment(2).'/*')) {
            $slug = Str::of($request->name)->slug('-');
            if (Article::where('slug', '=', $slug)->exists()) {
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.slug_duplicated'))->withInput($request->except('password'));
            }else{ 
                $properties = array();
                $properties['avatar'] = $request->avatar?$request->avatar:"";
                $properties['keywords'] = $request->keywords?$request->keywords:"";
                $properties['details'] = $request->details?$request->details:"";
                $properties['sapo'] = $request->sapo?$request->sapo:"";
                $item = Article::create([
                    'user_id' => $request->user()->id, 
                    'pid' => $request->pid?$request->pid:0,
                    'name' => $request->name?$request->name:"",
                    'slug' => $slug,
                    'display_date' => Carbon::parse($request->display_date)->format('Y-m-d H:i:s'),
                    'position' => $request->position?$request->position:0,
                    'properties' => serialize($properties),
                    'status' => $request->status?$request->status:1,
                    'created_at' => Carbon::now()
                ]);
                if($item){
                    $this->storeTracking(__('messages.add_new_article_success',['id' => $item->id]));
                    return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.add_new_success'));
                }
            }
        }
        return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
    }
    public function show($id){
        try {
            $item = Article::find($id);
            $list = ArticleCategories::select('*')->get(); 
            if($item){
                $properties = $item->getProperties();
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_show',['item' => $item,'properties' => $properties,'list' => $list]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.item_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function edit(Request $request)
    {
        try {
            $slug = Str::of($request->slug)->slug('-');
            if (Article::where('slug', '=', $slug)->where('id', '<>', $request->id)->exists()) {
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('error', __('messages.email_exist'))->withInput($request->except('password'));
            }else{
                $item = Article::find($request->id);
                $properties = array();
                if(!empty($item->properties)){
                    $properties = $item->getProperties();
                }
                $properties['avatar'] = $request->avatar?$request->avatar:"";
                $properties['keywords'] = $request->keywords?$request->keywords:"";
                $properties['details'] = $request->details?$request->details:"";
                $properties['sapo'] = $request->sapo?$request->sapo:"";
                $data = array(
                    'name' => $request->name,
                    'pid' => $request->pid?$request->pid:0,
                    'slug' => $slug,
                    'display_date' => Carbon::parse($request->display_date)->format('Y-m-d H:i:s'),
                    'position' => $request->position?$request->position:0,
                    'properties' => serialize($properties),
                    'status' => $request->status,
                );
                $item->update($data);
                $this->storeTracking(__('messages.update_article_success',['id' => $item->id]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('success', __('messages.update_success'));
            }
            } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function changeStatus(Request $request)
    {
        try {
                $item = Article::find($request->id);
                $item->update(['status'   => $request->status]);
                $this->storeTracking(__('messages.change_article_status',['id' => $request->id,'status' => config('constants.status.'.$request->status)]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success_id', ['id' => $request->id]));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function update(Request $request)//update status for checked items
    {
        try {
            $newStatus = $request->new_status?$request->new_status:"0";
            if(!empty($request->ids)){
                foreach ($request->ids as $id) {
                    $item = Article::find($id);
                    $item->update(['status' => $newStatus]);
                    $this->storeTracking(__('messages.change_article_status',['id' => $id,'status' => config('constants.status.'.$newStatus)]));
                }
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.no_item_selected'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function clean(){
        //return cleantrash view
        return view('/'.config('constants.adminLink').'/include/clean_trash');
    }
    public function destroy(){
        // delete all items with deleted status
        Article::select('*')->where('status','=',config('constants.delete'))->delete();
        $this->storeTracking(__('messages.clean_article_list'));
        return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.clean_trash_success'));
    }
}
