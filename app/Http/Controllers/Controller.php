<?php

namespace App\Http\Controllers;

use App\Models\Tracking;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests; 
    public function getIpAddress(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); //return server ip when no client ip found
    }
    public function storeTracking($action = '')
    {
        if(!isset($action)) return 0;
        $newTracking = Tracking::create([
            'user_id' => Auth::user()->id,
            'username' => Auth::user()->name,
            'action' => $action,
            'properties' => serialize(array('cur_url' => URL::current())),
            'status' => 1,
            'created_at' => Carbon::now()
        ]);
        if($newTracking){
            return 1;
        }
        return 0;
    }
}
