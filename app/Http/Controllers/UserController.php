<?php

namespace App\Http\Controllers;
use Exception;
use App\constants;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Tracking;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\ContactForm;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Notifications\RegisterUser;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Notifications\Notifiable;

class UserController extends Controller 
{
    use Notifiable;
    public function index(){
        // var_dump(request()->segment(2));
        // var_dump(URL::current());
        // var_dump(Route::getFacadeRoot()->current()->uri());
        // echo __('messages.welcome'); text language
        // $users = DB::table('users')->select('*')->get();
        // $list = collect($users)->map(function ($item) {
        //     return [
        //         "id"            => $item->id,
        //         "name"          => $item->name,
        //         "email"         => $item->email,
        //         "password"      => $item->password, 
        //         "created_at"    => Carbon::parse($item->created_at)->format('d-m-Y H:i:s'),
        //         "updated_at"    => Carbon::parse($item->updated_at)->format('d-m-Y H:i:s'),
        //         "status"        => $item->status,
        //         "statustextbackend"    => Constants::$status[$item->status],
        //         "home"          => $item->home,
        //     ]; 
        // });
        // var_dump($list);'list' => $totalPages->items(), 
        // $role = Auth::user()->hasRole('user');
        // var_dump(Route::currentRouteName());
        // var_dump($this->getIpAddress());// function from controller
        try {
            $this->storeTracking(__('messages.view_user_list'));// add tracking
            $totalPages = User::select('*')->orderBy("created_at", "DESC")->paginate(config('constants.itemPerPage'));// total page with condition and item per page. ->where('status','=',1) for condition
            return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2), ['list' => $totalPages]);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        } 
    } 
    public function trackings(){
        // $this->storeTracking(__('messages.view_tracking_list'));// add tracking
        $totalPages = Tracking::select('*')->orderBy("created_at", "DESC")->paginate(config('constants.itemPerPage'));// total page with condition and item per page. ->where('status','=',1) for condition
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_tracking', ['list' => $totalPages]);
    }
    public function create(){
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_create');// view folder name with 's' . file without 's'
    }  
    public function store(RegisterRequest $request)
    {
        if ($request->is(config('constants.adminLink').'/'.request()->segment(2).'/*')) {
            if (User::where('email', '=', $request->email)->exists()) {
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.email_exist'))->withInput($request->except('password'));
            }else{
                $properties = array();
                $properties['avatar'] = $request->avatar?$request->avatar:"";
                $user = User::create([
                    'user_id' => $request->user()->id,
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'level' => $request->level?$request->level:1,
                    'status' => $request->status,
                    'properties' => serialize($properties),
                    'created_at' => Carbon::now()
                ]);
                if($user){
                    $this->storeTracking(__('messages.add_new_user_success',['id' => $user->id]));
                    $user->update(array('api_token' => $user->id.Str::random(59)));// update token to user
                    return redirect()->route('user.index')->with('success', __('messages.add_new_success'));// return with route name
                    //return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.add_new_success'));
                }else{
                    $this->storeTracking(__('messages.add_new_user_failed'));
                    return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
                }
            } 
        }
        return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
    }
    public function show($id){
        try {
            // return $this->service->delete($id);
            $item = User::find($id);
            if($item){
                $properties = $item->getProperties();
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_show',['item' => $item,'properties' => $properties]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.user_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function edit(UserUpdateRequest $request)
    { 
        try {
            if (User::where('email', '=', $request->email)->where('id', '<>', $request->id)->exists()) {
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('error', __('messages.email_exist'))->withInput($request->except('password'));
            }else{
                $item = User::find($request->id);
                $properties = array();
                if(!empty($item->properties)){
                    $properties = $item->getProperties();
                }
                $properties['avatar'] = $request->avatar?$request->avatar:"";
                $data = array(
                    'level'    => $request->level?$request->level:1,
                    'name'     => $request->name,
                    'email'    => $request->email,
                    'properties' => serialize($properties),
                    'status'   => $request->status
                ); 
                if(!empty($request->password)){
                    $data['password'] = bcrypt($request->password);
                }
                $item->update($data);
                // var_dump($properties);exit();
                $this->storeTracking(__('messages.update_user_success',['id' => $item->id]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('success', __('messages.update_success'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function changeStatus(Request $request) 
    {
        try {
                $item = User::find($request->id);
                $item->update(['status'   => $request->status]);
                $this->storeTracking(__('messages.change_user_status',['id' => $request->id,'status' => config('constants.status.'.$request->status)]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success_id', ['id' => $request->id]));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function update(Request $request)//update status for checked items
    {
        try {
            $newStatus = $request->new_status?$request->new_status:"0";
            if(!empty($request->ids)){
                foreach ($request->ids as $id) {
                    $item = User::find($id);
                    $item->update(['status' => $newStatus]);
                    $this->storeTracking(__('messages.change_user_status',['id' => $id,'status' => config('constants.status.'.$newStatus)]));
                }
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.no_item_selected'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public static function list(){
        $list  = User::where('status', '1');
        return $list->get();
    }
    public function permissionUpdate(Request $request)
    { 
        try {
                $item = User::find($request->id);
                $item->syncPermissions();#reset all permissions
                $namePermission = '';
                if($request->permission){
                    foreach ($request->permission as $key => $value) {
                        $item->givePermissionTo($key);
                        $namePermission .= $key.', ';
                    }
                }
                $this->storeTracking(__('messages.update_user_permission',['id' => $request->id,'permission' => $namePermission]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2).'/permission/'.$request->id)->with('success', __('messages.update_success'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function permission($id){ 
        try {
            // return $this->service->delete($id);
            $item = User::find($id);
            if($item){
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'s/'.request()->segment(2).'_permission',['item' => $item]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.user_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function clean(){
        //return cleantrash view
        
        return view('/'.config('constants.adminLink').'/include/clean_trash');
    }
    public function destroy(){
        // delete all items with deleted status
        $this->storeTracking(__('messages.clean_user_list'));
        User::select('*')->where('status','=',config('constants.delete'))->delete();
        return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.clean_trash_success'));
    }
    // public function sendNotiToDefaultUser($object)
    // {
    //     if(!isset($object)) return 0;
    //     $admin = User::where('email','=',config('constants.defaultEmailReceive'))->first();
    //     if($admin){
    //         $admin->notify(new ContactForm($object));
    //         return 1;
    //     }
    //     return '0';
    // }
}
