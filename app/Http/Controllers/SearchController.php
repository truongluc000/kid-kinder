<?php

namespace App\Http\Controllers;
use Exception;
use App\constants;
use Carbon\Carbon;
use App\Models\Ads;
use App\Models\Menu;
use App\Models\User;
use App\Models\Article;
use App\Models\Classes;
use App\Models\Contact;
use App\Models\Gallery;
use App\Models\Statics;
use App\Models\Teacher;
use App\Models\Comments;
use App\Models\Tracking;
use Illuminate\Http\Request;
use App\Models\AdsCategories;
use App\Models\ArticleCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\UserUpdateRequest;

class SearchController extends Controller
 
{
    public function search(Request $request){
        $pId        = $request->pId;
        $keyword    = $request->kw;
        $object     = $request->object;
        $urlView = config('constants.adminLink');
        switch ($object) {
            case 'user':
                //->orWhere(), condition in 'where' or 'orWhere'
                //->whereBetween('column', ['value1','value2']) data between
                //->whereNotNull('column') column must have data
                //->select('title)->distinct() data without duplicated title
                if($pId != '0'){
                    $list = DB::table($object.'s')
                        ->where('level', $pId)
                        ->where('name', 'LIKE', '%'.$keyword.'%')
                        ->orWhere(function ($query) use ($keyword) { 
                            $query->where('email', 'LIKE', '%'.$keyword.'%');
                        })
                        ->orderBy("created_at", "DESC")
                        ->paginate(config('constants.itemPerPage'));
                }else{
                    $list = DB::table($object.'s')
                        ->where('name', 'LIKE', '%'.$keyword.'%')
                        ->orWhere(function ($query) use ($keyword) { 
                            $query->where('email', 'LIKE', '%'.$keyword.'%');
                        })
                        ->orderBy("created_at", "DESC")
                        ->paginate(config('constants.itemPerPage'));
                }
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break; 
            case 'menu':
                $list = Menu::select('*')
                    // ->where('pid', $pId)
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'classes':
                $list = Classes::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'teacher':
                $list = Teacher::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'gallery':
                $list = Gallery::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'/'.$object;
            break;
            case 'adscategories':
                $list = AdsCategories::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'/'.$object;
            break;
            case 'ads':
                $list = Ads::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'/'.$object;
            break;
            
            case 'articlecategories':
                $list = ArticleCategories::with('listItem')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'/'.$object;
            break;
            case 'article':
                $list = Article::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'static':
                $list = Statics::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'comment':
                $list = Comments::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'contact':
                $list = Contact::select('*')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
            case 'trackings':
                $list = Tracking::select('*')
                    ->where('username', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('action', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                // $urlView = route('user.trackings');
                $urlView = config('constants.adminLink').'/users/user_tracking';
            break;
            // for all table end with 1 s such as: teachers, menus ... and search with field name only
            default:
                $list = DB::table($object.'s')
                    ->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orderBy("created_at", "DESC")
                    ->paginate(config('constants.itemPerPage'));
                $urlView = config('constants.adminLink').'/'.$object.'s/'.$object;
            break;
        }
        if(empty($list)) {
            return redirect(config('constants.adminLink').'/'.$object.'s/'.$object)->with('warning', __('messages.no_result'));
        }else{
            return view($urlView, ['list' => $list]);
        }
    }

}
