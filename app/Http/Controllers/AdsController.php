<?php

namespace App\Http\Controllers;
use Exception;
use Carbon\Carbon;
use App\Models\Ads;
use Illuminate\Http\Request;
use App\Models\AdsCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AdsController extends Controller
 
{
    public function index(){
        try {
            $list = Ads::select('*')->orderBy("created_at", "DESC")->paginate(config('constants.itemPerPage'));// total page with condition and item per page. ->where('status','=',1) for condition
            return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2), ['list' => $list]);
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    } 
    public function create(){
        $list = AdsCategories::select('*')->get();
        return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2).'_create', ['list' => $list]);// view folder name with 's' . file without 's'
    }  
    public function store(Request $request)
    {
        if ($request->is(config('constants.adminLink').'/'.request()->segment(2).'/*')) {
            $properties = array();
            $properties['url'] = $request->url?$request->url:"";
            $properties['avatar'] = $request->avatar?$request->avatar:"";
            $properties['sapo'] = $request->sapo?$request->sapo:"";
            $properties['keywords'] = $request->keywords?$request->keywords:"";
            $properties['css_class'] = $request->css_class?$request->css_class:"";
            
            $item = Ads::create([
                'user_id' => $request->user()->id,
                'pid' => $request->pid?$request->pid:0,
                'name' => $request->name?$request->name:"",
                'position' => $request->position?$request->position:0,
                'properties' => serialize($properties),
                'status' => $request->status?$request->status:1,
                'created_at' => Carbon::now()
            ]);
            if($item){
                $this->storeTracking(__('messages.add_new_ads_success',['id' => $item->id]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.add_new_success'));
            }
            // else{
            //     return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
            // }
        }
        return redirect(config('constants.adminLink').'/'.request()->segment(2).'/create')->with('error', __('messages.failed'))->withInput($request->except('password'));
    }
    public function show($id){
        try { 
            $item = Ads::find($id);
            if($item){
                $list = AdsCategories::select('*')->get();
                $properties = $item->getProperties();
                return view('/'.config('constants.adminLink').'/'.request()->segment(2).'/'.request()->segment(2).'_show',['item' => $item,'properties' => $properties,'list' => $list]);
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.item_not_exist'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        
    } 
    public function edit(Request $request)
    {
        try {
            $item = Ads::find($request->id);
            $properties = array();
            if(!empty($item->properties)){
                $properties = $item->getProperties();
            }
            $properties['url'] = $request->url?$request->url:"";
            $properties['avatar'] = $request->avatar?$request->avatar:"";
            $properties['sapo'] = $request->sapo?$request->sapo:"";
            $properties['keywords'] = $request->keywords?$request->keywords:"";
            $properties['css_class'] = $request->css_class?$request->css_class:"";

            $data = array(
                'pid' => $request->pid?$request->pid:0,
                'name' => $request->name?$request->name:"",
                'position' => $request->position?$request->position:0,
                'properties' => serialize($properties),
                'status' => $request->status,
            );
            $item->update($data);
            $this->storeTracking(__('messages.update_ads_success',['id' => $item->id]));
            return redirect(config('constants.adminLink').'/'.request()->segment(2).'/show/'.$request->id)->with('success', __('messages.update_success'));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function changeStatus(Request $request)
    {
        try {
                $item = Ads::find($request->id);
                $item->update(['status'   => $request->status]);
                $this->storeTracking(__('messages.change_ads_status',['id' => $request->id,'status' => config('constants.status.'.$request->status)]));
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success_id', ['id' => $request->id]));
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function update(Request $request)//update status for checked items
    {
        try {
            $newStatus = $request->new_status?$request->new_status:"0";
            if(!empty($request->ids)){
                foreach ($request->ids as $id) {
                    $item = Ads::find($id);
                    $item->update(['status' => $newStatus]);
                    $this->storeTracking(__('messages.change_ads_status',['id' => $id,'status' => config('constants.status.'.$newStatus)]));
                }
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.update_success'));
            }else{
                return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('error', __('messages.no_item_selected'));
            }
        } catch (Exception $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
    }
    public function clean(){
        //return cleantrash view
        return view('/'.config('constants.adminLink').'/include/clean_trash');
    }
    public function destroy(){
        // delete all items with deleted status
        Ads::select('*')->where('status','=',config('constants.delete'))->delete();
        $this->storeTracking(__('messages.clean_ads_list'));
        return redirect(config('constants.adminLink').'/'.request()->segment(2))->with('success', __('messages.clean_trash_success'));
    }
}
