<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:20',
            'email' => 'required|email',
            'message' => 'required|min:20|max:800',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'name.min' => 'Minimum length is 3 letters!',
            'name.max' => 'Maximum length is 20 letters!',
            'email.required' => 'Email is required!',
            'message.min' => 'Minimum length is 20 letters!',
            'message.max' => 'Maximum length is 800 letters!',
        ];
    }
}
