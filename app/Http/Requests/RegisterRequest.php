<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{ 
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:10',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'name.min' => 'Minimum length is 3 letters!',
            'name.max' => 'Maximum length is 10 letters!',
            'email.required' => 'Email is required!',
            'password.required' => 'Password is required!',
            'password.min' => 'Minimun length is 6 letters!',
            'password.confirmed' => 'Password not match!',
            'password_confirmation.required' => 'Password confirmation is required!',
        ];
    }
}
