<?php
namespace App\Services;

use App\Models\Statics;
use App\Exceptions\StaticNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StaticService
{
    public function findBySlug($slug)
    {
        $object = Statics::where('slug','=', $slug)->where('status',1)->first();
        if(!$object){
            throw new StaticNotFoundException('static not found by slug: '.$slug); // message error
        }
        return $object;
    }
}