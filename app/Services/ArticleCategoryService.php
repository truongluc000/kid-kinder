<?php
namespace App\Services;

use App\Models\ArticleCategories;
use App\Exceptions\ArticleCategoryNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ArticleCategoryService
{
    public function findBySlug($slug)
    {
        $object = ArticleCategories::where('slug','=', $slug)->where('status',1)->first();
        if(!$object){
            throw new ArticleCategoryNotFoundException('category not found by slug: '.$slug); // message error
        }
        return $object;
    }
}