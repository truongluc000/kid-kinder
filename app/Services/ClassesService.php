<?php
namespace App\Services;

use App\Models\Classes;
use App\Exceptions\ClassesNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ClassesService
{
    public function findBySlug($slug)
    {
        $object = Classes::where('slug','=', $slug)->where('status',1)->first();
        if(!$object){
            throw new ClassesNotFoundException('classes not found by slug: '.$slug); // message error
        }
        return $object; 
    }
}