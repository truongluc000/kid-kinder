<?php
namespace App\Services;

use App\Models\Article;
use App\Exceptions\ArticleNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ArticleService
{
    public function findBySlug($slug)
    {
        $object = Article::with(['categories','author'])->where('slug','=', $slug)->where('status',1)->first();
        if(!$object){
            throw new ArticleNotFoundException('article not found by slug: '.$slug); // message error
        }
        return $object; 
    }
}