<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConfigGeneral extends Model
{
    use HasFactory;
    protected $table = 'config_general';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'position',  
        'properties',
        'status',
    ];
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        return isset($properties[$key]) ? $properties[$key] : null;
    }
}