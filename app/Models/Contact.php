<?php

namespace App\Models;

use App\Models\Classes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    use HasFactory;
    protected $table = 'contacts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'properties',
        'status',
        'type',
        'classes',
    ];
    protected $hidden = [
        'remember_token'
    ];
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        return isset($properties[$key]) ? $properties[$key] : null;
    }
    public function getDateCreated()
    {
        $formatDate = $this->created_at->format('d M Y').' at '.$this->created_at->format('h:i a');
        return $formatDate;
    }
    public function classRoom() 
    {
        return $this->belongsTo(Classes::class, "classes");
    }
}