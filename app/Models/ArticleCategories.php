<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ArticleCategories extends Model
{
    use HasFactory;
    protected $table = 'article_categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'pid',
        'name',
        'slug',
        'position',  
        'properties',
        'status',
    ];
    public function listItem()
    {
        return $this->hasMany(Article::class,'pid','id')->where('status','=','1'); 
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
} 
