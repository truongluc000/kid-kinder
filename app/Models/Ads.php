<?php

namespace App\Models;

use App\Models\User;
use App\Models\AdsCategories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ads extends Model
{
    use HasFactory;
    protected $table = 'ads';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'pid',
        'name',
        'sapo',
        'position',  
        'properties',
        'status',
    ];
    public function categories()
    {
        return $this->belongsTo(AdsCategories::class, "pid");//
    }
    public function author()
    {
        return $this->belongsTo(User::class, "user_id"); 
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
}
