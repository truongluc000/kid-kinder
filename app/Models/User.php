<?php

namespace App\Models; 

use App\Models\Article;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable; 

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'password',
        'level',
        'status',
        'home',
        'properties',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token', 
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function listArticle()
    {
        return $this->hasMany(Article::class,'user_id','id');  
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
    public function getRole($key = '')
    {
        if(!$key) return '';
        if($this->hasRole($key)) return 1;
        return 0;
    }
    
}
