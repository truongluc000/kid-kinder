<?php

namespace App\Models;

use App\Models\Ads;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdsCategories extends Model
{
    use HasFactory;
    protected $table = 'ads_categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'position',  
        'properties',
        'status',
    ];
    public function listItem()
    {
        return $this->hasMany(Ads::class,'pid','id')->where('status','=','1'); 
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
}
