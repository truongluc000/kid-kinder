<?php

namespace App\Models;

use App\Models\User;
use App\Models\Comments;
use App\Models\ArticleCategories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'pid',
        'name',
        'slug',
        'display_date',
        'position',  
        'properties',
        'status',
    ];
    // an article belong to a category
    public function categories() 
    {
        return $this->belongsTo(ArticleCategories::class, "pid");
    }
    public function author()
    {
        return $this->belongsTo(User::class, "user_id"); 
    }
    public function listComments() 
    {
        return $this->hasMany(Comments::class,'article_id','id')->where('status','=','1'); 
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    } 
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
}