<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $table = 'teachers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name',
        'sapo',
        'position', 
        'properties',
        'status',
    ];
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        if($key == 'avatar' && (!isset($properties[$key]) || $properties[$key] == '')) return config('constants.defaultImage');
        return isset($properties[$key]) ? $properties[$key] : null;
    }
}
