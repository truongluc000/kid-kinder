<?php

namespace App\Models;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comments extends Model
{
    use HasFactory;
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'article_id',
        'pid',
        'name',
        'properties',
        'status',
    ];
    protected $hidden = [
        'remember_token'
    ];
    public function article() 
    {
        return $this->belongsTo(Article::class, "article_id");
    }
    public function getProperties()
    {
        if(!$this->properties) return null;
        return unserialize($this->properties);
    }
    public function getProperty($key = '')
    {
        if(!$key) return null;
        $properties = $this->getProperties();
        return isset($properties[$key]) ? $properties[$key] : null;
    }
    public function getDateCreated()
    {
        $formatDate = $this->created_at->format('d M Y').' at '.$this->created_at->format('h:i a');
        return $formatDate;
    }
}