<?php

namespace Database\Seeders;

use App\Models\Statics;
use Illuminate\Database\Seeder;

class StaticsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Learn about us',
                'slug'      => 'about-us',
                'position'  => '1',
                'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/about/about-1.jpg','keywords' => 'Best School For Your Kids','sapo' => 'Invidunt lorem justo sanctus clita. Erat lorem labore ea, justo dolor lorem ipsum ut sed eos, ipsum et dolor kasd sit ea justo. Erat justo sed sed diam. Ea et erat ut sed diam sea ipsum est dolor','details'=>'<div class="col-6 col-md-4"><img class="img-fluid rounded" src="http://kid-kinder.test/userfiles/images/about/about-2.jpg" alt=""></div> <div class="col-6 col-md-8"> <ul class="list-inline m-0"> <li class="py-2 border-top border-bottom"><i class="fa fa-check text-primary mr-3"></i>Labore eos amet dolor amet diam</li> <li class="py-2 border-bottom"><i class="fa fa-check text-primary mr-3"></i>Etsea et sit dolor amet ipsum</li> <li class="py-2 border-bottom"><i class="fa fa-check text-primary mr-3"></i>Diam dolor diam elitripsum vero.</li> </ul> </div>')),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Get in touch',
                'slug'      => 'contact',
                'position'  => '2',
                'properties'=> serialize(array('keywords' => 'Contact Us For Any Query','sapo' => 'Labore sea amet kasd diam justo amet ut vero justo. Ipsum ut et kasd duo sit, ipsum sea et erat est dolore, magna ipsum et magna elitr. Accusam accusam lorem magna, eos et sed eirmod dolor est eirmod eirmod amet.')),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'Popular Classes',
                'slug'      => 'our-class',
                'position'  => '3',
                'properties'=> serialize(array('keywords' => 'Classes for Your Kids','sapo' => '','details' => '<p class="section-title pr-5"><span class="pr-2">Book A Seat</span></p> <h1 class="mb-4">Book A Seat For Your Kid</h1> <p>Invidunt lorem justo sanctus clita. Erat lorem labore ea, justo dolor lorem ipsum ut sed eos, ipsum et dolor kasd sit ea justo. Erat justo sed sed diam. Ea et erat ut sed diam sea ipsum est dolor</p> <ul class="list-inline m-0"> <li class="py-2"><i class="fa fa-check text-success mr-3"></i>Labore eos amet dolor amet diam</li> <li class="py-2"><i class="fa fa-check text-success mr-3"></i>Etsea et sit dolor amet ipsum</li> <li class="py-2"><i class="fa fa-check text-success mr-3"></i>Diam dolor diam elitripsum vero.</li> </ul>')),
                'status'    => '1' 
            ],
            [
                'id'        => 4,
                'user_id'   => 1,
                'name'      => 'Our Teachers',
                'slug'      => 'teachers',
                'position'  => '4',
                'properties'=> serialize(array('keywords' => 'Meet Our Teachers','sapo' => '','details' => '')),
                'status'    => '1' 
            ],
            [
                'id'        => 5,
                'user_id'   => 1,
                'name'      => 'Gallery',
                'slug'      => 'gallery',
                'position'  => '5',
                'properties'=> serialize(array('keywords' => 'Our Kids School Gallery','sapo' => '','details' => '')),
                'status'    => '1' 
            ],
            [
                'id'        => 6,
                'user_id'   => 1,
                'name'      => 'Latest Blog',
                'slug'      => 'blogs',
                'position'  => '6',
                'properties'=> serialize(array('keywords' => 'Latest Articles From Blog','sapo' => '','details' => '')),
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = Statics::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
