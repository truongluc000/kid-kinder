<?php

namespace Database\Seeders;

use App\Models\Classes;
use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Drawing Class',
                'position'  => '1',
                'properties'=> serialize(array('sapo' => 'Justo ea diam stet diam ipsum no sit, ipsum vero et et diam ipsum duo et no et, ipsum ipsum erat duo amet clita duo','avatar' => 'http://kid-kinder.test/userfiles/images/Classes/class-1.jpg','age' => '3 - 6 Years','total' => '40 Seats','time' => '08:00 - 10:00','fee' => '$290 / Month')),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Language Learning',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Justo ea diam stet diam ipsum no sit, ipsum vero et et diam ipsum duo et no et, ipsum ipsum erat duo amet clita duo','avatar' => 'http://kid-kinder.test/userfiles/images/Classes/class-2.jpg','age' => '3 - 6 Years','total' => '40 Seats','time' => '08:00 - 10:00','fee' => '$290 / Month')),
                'status'    => '1'
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'Basic Science',
                'position'  => '3',
                'properties'=> serialize(array('sapo' => 'Justo ea diam stet diam ipsum no sit, ipsum vero et et diam ipsum duo et no et, ipsum ipsum erat duo amet clita duo','avatar' => 'http://kid-kinder.test/userfiles/images/Classes/class-3.jpg','age' => '3 - 6 Years','total' => '40 Seats','time' => '08:00 - 10:00','fee' => '$290 / Month')),
                'status'    => '1'
            ]
        ];
        foreach ($arrays as $item) {
            $item = Classes::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
