<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'name'      => 'John Doe',
                'properties'=> serialize(array(
                    'message' => 'Labore dolor amet ipsum ea, erat sit ipsum duo eos. Volup amet ea dolor et magna dolor, elitr rebum duo est sed diam elitr. Stet elitr stet diam duo eos rebum ipsum diam ipsum elitr.',
                    'email' => 'info@example.com',
                    'subject' => 'Contact for information',
                )),
                'status'    => '1', 
                'type'      => '0'
            ],
            [
                'id'        => 2,
                'name'      => 'John Doe',
                'properties'=> serialize(array('email' => 'info@example.com')),
                'status'    => '1', 
                'type'      => '1'
            ],
            [
                'id'        => 3,
                'name'      => 'John Doe',
                'properties'=> serialize(array('email' => 'info@example.com')),
                'status'    => '1', 
                'type'      => '2',
                'classes'   => '1'
            ]
            ,
            [
                'id'        => 7,
                'name'      => 'John Doe',
                'properties'=> serialize(array('email' => 'info@example.com')),
                'status'    => '1', 
                'type'      => '2',
                'classes'   => '1'
            ]
        ];
        foreach ($arrays as $item) {
            $item = Contact::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
