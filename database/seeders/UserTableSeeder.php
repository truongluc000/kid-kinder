<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listAdmin = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Ersil',
                'email'     => 'truongluc000@gmail.com',
                'password'  => bcrypt("123456"),
                'level'     => '1',
                'status'    => '1',
                'home'      => '1',
                'properties'=> serialize(array('avatar' => '/userfiles/images/Users/avt-2.jpg')),
                'api_token' => '1'.Str::random(59),

            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Truong Luc',
                'email'     => 'truongluc.info@gmail.com',
                'password'  => bcrypt("123456"),
                'level'     => '2',
                'status'    => '1', 
                'home'      => '0',
                'properties'=> serialize(array('avatar' => '/userfiles/images/Users/avt-2.jpg')),
                'api_token' => '2'.Str::random(59),

            ]
        ];
        foreach ($listAdmin as $admin) {
            $user = User::query()->updateOrCreate(["id" => $admin["id"]], $admin)->assignRole('admin');
        }
        $users = [
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'David',
                'email'     => 'david@gmail.com',
                'password'  => bcrypt("123456"),
                'level'     => '1',
                'status'    => '1', 
                'home'      => '0',
                'properties'=> serialize(array('avatar' => '/userfiles/images/Users/avt-2.jpg')),
                'api_token' => '3'.Str::random(59),

            ]
        ];
        foreach ($users as $user) {
            $user = User::query()->updateOrCreate(["id" => $user["id"]], $user)->assignRole('user');
        }
    }
}
