<?php

namespace Database\Seeders;

use App\Models\ArticleCategories;
use Illuminate\Database\Seeder;

class ArticleCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'pid'       => 0,
                'name'      => 'Web Design',
                'slug'      => 'web-design',
                'position'  => '1',
                'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-1.jpg','sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero')),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'pid'       => 0,
                'name'      => 'Web Development',
                'slug'      => 'web-development',
                'position'  => '2',
                'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-1.jpg','sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero')),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'pid'       => 0,
                'name'      => 'Online Marketing',
                'slug'      => 'online-marketing',
                'position'  => '3',
                'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-1.jpg','sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero')),
                'status'    => '1' 
            ], 
            [
                'id'        => 4,
                'user_id'   => 1,
                'pid'       => 0,
                'name'      => 'Keyword Research',
                'slug'      => 'keyword-research',
                'position'  => '4',
                'properties'=> serialize(array('avatar' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-1.jpg','sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero')),
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = ArticleCategories::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
