<?php

namespace Database\Seeders;

use App\Models\Teacher;
use Illuminate\Database\Seeder;

class TeacherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Julia Smith',
                'position'  => '1',
                'properties'=> serialize(array('sapo' => 'Music Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-1.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Jhon Doe',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Language Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-2.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'Mollie Ross',
                'position'  => '3',
                'properties'=> serialize(array('sapo' => 'Dance Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-3.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 4,
                'user_id'   => 1,
                'name'      => 'Donald John',
                'position'  => '4',
                'properties'=> serialize(array('sapo' => 'Art Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-4.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 5,
                'user_id'   => 1,
                'name'      => 'David Smith',
                'position'  => '5',
                'properties'=> serialize(array('sapo'=> 'Music Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-1.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 6,
                'user_id'   => 1,
                'name'      => 'JungHaeJin',
                'position'  => '6',
                'properties'=> serialize(array('sapo'=> 'Act Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-2.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 7,
                'user_id'   => 1,
                'name'      => 'Lyvia Dean',
                'position'  => '7',
                'properties'=> serialize(array('sapo' => 'Music Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-3.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
            [
                'id'        => 8,
                'user_id'   => 1,
                'name'      => 'Paus Donald',
                'position'  => '8',
                'properties'=> serialize(array('sapo'=> 'Music Teacher','avatar' => 'http://kid-kinder.test/userfiles/images/Teachers/team-4.jpg','twitter' => '#','facebook' => '#','linkedin' => '#')),
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = Teacher::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
