<?php

namespace Database\Seeders;

use App\Models\Comments;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'article_id'=> 1,
                'pid'       => 0,
                'name'      => 'John Doe',
                'properties'=> serialize(array(
                    'message' => 'Labore dolor amet ipsum ea, erat sit ipsum duo eos. Volup amet ea dolor et magna dolor, elitr rebum duo est sed diam elitr. Stet elitr stet diam duo eos rebum ipsum diam ipsum elitr.',
                    'email' => 'info@example.com',
                )),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'article_id'=> 1,
                'pid'       => 0,
                'name'      => 'John Dee',
                'properties'=> serialize(array(
                    'message' => 'Labore dolor amet ipsum ea, erat sit ipsum duo eos.',
                    'email' => 'info2@example.com',
                )),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'article_id'=> 1,
                'pid'       => 0,
                'name'      => 'John Doe',
                'properties'=> serialize(array(
                    'message' => 'Volup amet ea dolor et magna dolor, elitr rebum duo est sed diam elitr.',
                    'email' => 'info@example.com',
                )),
                'status'    => '1' 
            ]
        ];
        foreach ($arrays as $item) {
            $item = Comments::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
