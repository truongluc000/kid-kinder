<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\AdsTableSeeder;
use Database\Seeders\MenuTableSeeder;
use Database\Seeders\RoleTableSeeder;
use Database\Seeders\UserTableSeeder;
use Database\Seeders\ArticleTableSeeder;
use Database\Seeders\ClassesTableSeeder;
use Database\Seeders\CommentTableSeeder;
use Database\Seeders\ContactTableSeeder;
use Database\Seeders\GalleryTableSeeder;
use Database\Seeders\StaticsTableSeeder;
use Database\Seeders\TeacherTableSeeder;
use Database\Seeders\TrackingTableSeeder;
use Database\Seeders\PermissionTableSeeder;
use Database\Seeders\AdsCategoriesTableSeeder;
use Database\Seeders\ConfigGeneralTableSeeder;
use Database\Seeders\ArticleCategoriesTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ConfigGeneralTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(ClassesTableSeeder::class);
        $this->call(TeacherTableSeeder::class);
        $this->call(AdsCategoriesTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(GalleryTableSeeder::class);
        $this->call(ArticleCategoriesTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(StaticsTableSeeder::class);
        $this->call(CommentTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(TrackingTableSeeder::class);
    }
}
