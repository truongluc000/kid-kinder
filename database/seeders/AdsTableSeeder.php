<?php

namespace Database\Seeders;

use App\Models\Ads;
use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'pid'       => 1,
                'name'      => 'Kids Learning Center',
                'position'  => '1',
                'properties'=> serialize(array('keywords' => 'New Approach to Kids Education','sapo' => 'The education your child receives will set the groundwork for future successes. No matter his age -- from kindergarten to high school -- we have all the advice and information you need on reaching potential in reading, writing, math, and more.','avatar' => 'http://kid-kinder.test/userfiles/images/sliders/header.png')),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Play Ground',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-050-fence')),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Music and Dance',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-022-drum')),
                'status'    => '1' 
            ],
            [
                'id'        => 4,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Arts and Crafts',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-030-crayons')),
                'status'    => '1' 
            ],
            [
                'id'        => 5,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Safe Transportation',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-017-toy-car')),
                'status'    => '1' 
            ],
            [
                'id'        => 6,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Healthy food',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-025-sandwich')),
                'status'    => '1' 
            ],
            [
                'id'        => 7,
                'user_id'   => 1,
                'pid'       => 2,
                'name'      => 'Educational Tour',
                'position'  => '2',
                'properties'=> serialize(array('sapo' => 'Kasd labore kasd et dolor est rebum dolor ut, clita dolor vero lorem amet elitr vero...','css_class' => 'flaticon-047-backpack')),
                'status'    => '1' 
            ],
            [
                'id'        => 8,
                'user_id'   => 1,
                'pid'       => 3,
                'name'      => 'Parent Name',
                'position'  => '2',
                'properties'=> serialize(array('keywords' => 'Profession','sapo' => 'Sed ea amet kasd elitr stet, stet rebum et ipsum est duo elitr eirmod clita lorem. Dolor tempor ipsum clita','avatar' => 'http://kid-kinder.test/userfiles/images/testimonial/testimonial-1.jpg')),
                'status'    => '1' 
            ],
            [
                'id'        => 9,
                'user_id'   => 1,
                'pid'       => 3,
                'name'      => 'Parent Name',
                'position'  => '2',
                'properties'=> serialize(array('keywords' => 'Profession','sapo' => 'Sed ea amet kasd elitr stet, stet rebum et ipsum est duo elitr eirmod clita lorem. Dolor tempor ipsum clita','avatar' => 'http://kid-kinder.test/userfiles/images/testimonial/testimonial-2.jpg')),
                'status'    => '1' 
            ],
            [
                'id'        => 10,
                'user_id'   => 1,
                'pid'       => 3,
                'name'      => 'Parent Name',
                'position'  => '2',
                'properties'=> serialize(array('keywords' => 'Profession','sapo' => 'Sed ea amet kasd elitr stet, stet rebum et ipsum est duo elitr eirmod clita lorem. Dolor tempor ipsum clita','avatar' => 'http://kid-kinder.test/userfiles/images/testimonial/testimonial-3.jpg')),
                'status'    => '1' 
            ],
            [
                'id'        => 11,
                'user_id'   => 1,
                'pid'       => 3,
                'name'      => 'Parent Name',
                'position'  => '2',
                'properties'=> serialize(array('keywords' => 'Profession','sapo' => 'Sed ea amet kasd elitr stet, stet rebum et ipsum est duo elitr eirmod clita lorem. Dolor tempor ipsum clita','avatar' => 'http://kid-kinder.test/userfiles/images/testimonial/testimonial-4.jpg')),
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = Ads::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
