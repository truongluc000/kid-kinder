<?php

namespace Database\Seeders;

use App\Models\AdsCategories;
use Illuminate\Database\Seeder;

class AdsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Index sliders',
                'position'  => '1',
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Facilities',
                'position'  => '2',
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'Testimonial',
                'properties'=> serialize(array('sapo' => 'What Parents Say!')),
                'position'  => '3', 
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = AdsCategories::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
