<?php

namespace Database\Seeders;

use App\Models\Tracking;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Request;

class TrackingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'username'  => 'Ersil',
                'action'    => 'Refresh Database',
                'status'    => '1', 
                'ip'        => ''
            ]
        ];
        foreach ($arrays as $item) {
            $item = Tracking::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
