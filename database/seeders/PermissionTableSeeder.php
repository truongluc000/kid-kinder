<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'view_user'],
            ['name' => 'add_user'],
            ['name' => 'edit_user'],
            ['name' => 'delete_user'],
            ['name' => 'permission_user'],

            ['name' => 'view_menu'],
            ['name' => 'add_menu'],
            ['name' => 'edit_menu'],
            ['name' => 'delete_menu'],

            ['name' => 'view_classes'],
            ['name' => 'add_classes'],
            ['name' => 'edit_classes'],
            ['name' => 'delete_classes'],

            ['name' => 'view_teacher'],
            ['name' => 'add_teacher'],
            ['name' => 'edit_teacher'],
            ['name' => 'delete_teacher'],

            ['name' => 'view_gallery'],
            ['name' => 'add_gallery'],
            ['name' => 'edit_gallery'],
            ['name' => 'delete_gallery'],

            ['name' => 'view_ads_category'],
            ['name' => 'add_ads_category'],
            ['name' => 'edit_ads_category'],
            ['name' => 'delete_ads_category'],

            ['name' => 'view_ads'],
            ['name' => 'add_ads'],
            ['name' => 'edit_ads'],
            ['name' => 'delete_ads'],

            ['name' => 'view_article_category'],
            ['name' => 'add_article_category'],
            ['name' => 'edit_article_category'],
            ['name' => 'delete_article_category'],

            ['name' => 'view_article'],
            ['name' => 'add_article'],
            ['name' => 'edit_article'],
            ['name' => 'delete_article'],

            ['name' => 'view_comment'],
            ['name' => 'add_comment'],
            ['name' => 'edit_comment'],
            ['name' => 'delete_comment'],

            ['name' => 'view_contact'],
            ['name' => 'add_contact'],
            ['name' => 'edit_contact'], 
            ['name' => 'delete_contact'],

            ['name' => 'view_static'],
            ['name' => 'add_static'],
            ['name' => 'edit_static'],
            ['name' => 'delete_static'],

            ['name' => 'view_library'],
            ['name' => 'add_library'],
            ['name' => 'edit_library'],
            ['name' => 'delete_library'],
        ];
        foreach ($permissions as $key => $permission) {
            Permission::query()->updateOrCreate(["id" => $key+1], $permission);
        }
    }
}
