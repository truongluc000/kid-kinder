<?php

namespace Database\Seeders;

use App\Models\ConfigGeneral;
use Illuminate\Database\Seeder;

class ConfigGeneralTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'KidKinder',
                'position'  => '1',
                'properties'=> serialize(array(
                    'name' => 'KidKinder',
                    'keywords' => 'New Approach to Kids Education',
                    'sapo' => 'Labore dolor amet ipsum ea, erat sit ipsum duo eos. Volup amet ea dolor et magna dolor, elitr rebum duo est sed diam elitr. Stet elitr stet diam duo eos rebum ipsum diam ipsum elitr.',
                    'copyright' => '© <a class="text-primary font-weight-bold" href="/">Kid Kinder</a>. All Rights Reserved. Designed by <a class="text-primary font-weight-bold" href="https://truongluc.com">TruongLuc</a>',
                    'email' => 'info@example.com',
                    'tel' => '012 345 67890',
                    'address' => '123 Street, New York, USA',
                    'twitter' => '#',
                    'facebook' => '#',
                    'linkedin' => '#',
                    'instagram' => '#',
                    'avatar' => 'http://kid-kinder.test/userfiles/images/logo/kid-logo.jpg',
                    'favicon' => 'http://kid-kinder.test/userfiles/images/logo/kid-circle.png',
                    'version_home' => '1',
                    'version_admin' => '1',
                    'opening_days' => 'Sunday - Friday',
                    'opening_hours' => '08:00 AM - 05:00 PM',
                )),
                'status'    => '1' 
            ]
        ];
        foreach ($arrays as $item) {
            $item = ConfigGeneral::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
