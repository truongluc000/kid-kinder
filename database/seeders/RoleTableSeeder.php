<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = Role::query()->updateOrCreate(["id" => 1], [
            'id'        => 1,
            'name'      => 'user',
            'guard_name'=> 'web',
        ]);
        // set permission to user role
        $userRole->givePermissionTo([
            'view_classes','add_classes','edit_classes',
            'view_teacher','add_teacher','edit_teacher',
            'view_gallery','add_gallery','edit_gallery',
            'view_article','add_article','edit_article',
        ]);
        // admin has full permissions so dont need to set
        Role::query()->updateOrCreate(["id" => 2], [
            'id'        => 2,
            'name'      => 'admin',
            'guard_name'=> 'web',
        ]);
    }
}
