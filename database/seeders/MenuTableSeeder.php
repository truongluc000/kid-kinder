<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'       => 1,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Home',
                'position' => '1',
                'properties'=> serialize(array('url' => '/')),
                'status'   => '1'
            ],
            [
                'id'       => 2,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'About',
                'position' => '2',
                'properties'=> serialize(array('url' => '/about-us.html')),
                'status'   => '1'
            ],
            [
                'id'       => 3,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Classes',
                'position' => '3',
                'properties'=> serialize(array('url' => '/our-class.html')),
                'status'   => '1'
            ],
            [
                'id'       => 4,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Teachers',
                'position' => '4',
                'properties'=> serialize(array('url' => '/teachers.html')),
                'status'   => '1'
            ],
            [
                'id'       => 5,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Gallery',
                'position' => '5',
                'properties'=> serialize(array('url' => '/gallery.html')),
                'status'   => '1'
            ],
            [
                'id'       => 6,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Blogs',
                'position' => '6',
                'properties'=> serialize(array('url' => '/blogs.html')),
                'status'   => '1'
            ],
            [
                'id'       => 7,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Novels',
                'position' => '7',
                'properties'=> serialize(array('url' => '/novel.html')),
                'status'   => '1'
            ],
            [
                'id'       => 8,
                'user_id'  => 1,
                'pid'      => 0,
                'name'     => 'Contact',
                'position' => '8',
                'properties'=> serialize(array('url' => '/contact.html')),
                'status'   => '1'
            ]
        ];
        foreach ($arrays as $item) {
            $item = Menu::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
