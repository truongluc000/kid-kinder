<?php

namespace Database\Seeders;

use App\Models\Gallery;
use Illuminate\Database\Seeder;

class GalleryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrays = [
            [
                'id'        => 1,
                'user_id'   => 1,
                'name'      => 'Playing',
                'position'  => '1',
                'properties'=> serialize(array('arrayPhoto' => array(array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-1.jpg','position'=>'1'),array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-2.jpg','position'=>'2')))),
                'status'    => '1' 
            ],
            [
                'id'        => 2,
                'user_id'   => 1,
                'name'      => 'Drawing',
                'position'  => '2',
                'properties'=> serialize(array('arrayPhoto' => array(array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-3.jpg','position'=>'1'),array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-4.jpg','position'=>'2')))),
                'status'    => '1' 
            ],
            [
                'id'        => 3,
                'user_id'   => 1,
                'name'      => 'Reading',
                'position'  => '3',
                'properties'=> serialize(array('arrayPhoto' => array(array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-5.jpg','position'=>'1'),array('photo' => 'http://kid-kinder.test/userfiles/images/Gallery/portfolio-6.jpg','position'=>'2')))),
                'status'    => '1' 
            ],
        ];
        foreach ($arrays as $item) {
            $item = Gallery::query()->updateOrCreate(["id" => $item["id"]], $item);
        }
    }
}
