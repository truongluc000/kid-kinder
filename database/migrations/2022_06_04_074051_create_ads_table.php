<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('pid')->default(0);
            $table->string('name');
            // $table->string('sapo');
            $table->integer('position')->default(0);
            $table->longText('properties')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->foreign('pid')->references('id')->on('article_categories')->onDelete('cascade');
        });
        // Schema::table('ads', function($table) {
        //     $table->foreign('pid')->references('id')->on('ads_categories')->onDelete('cascade');//or onDelete('set null')
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
