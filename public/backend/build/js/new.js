// environment.data.append('_token', token); //token head
function changeStatus(newstatus){ // change status for checked item in list
    if (confirm('Please confirm if you want to do this?')) {
        document.getElementById("new_status").value = newstatus;
        setTimeout(function(){document.getElementById("formType").submit();}, 500);
    } 
}
function submitFormSearch() {//submit search for all controller
    let href = window.location.href;
    document.getElementById("formTypesearch").action = href+"/search";
    setTimeout(function(){document.getElementById("formTypesearch").submit();}, 500);
}
function selectFileWithCKFinder( elementId ) { //load ckfinder popup
	CKFinder.popup( {
        language: 'vi',
		chooseFiles: true, 
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
				output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		} 
	} );
}
function openPhotoUpload(ob){// select photo ckfinder when click button
	CKFinder.popup( {//or .modal
        language: 'vi',
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				$(ob).parents(".form-group").find(".select_photo").val(file.getUrl());
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				$(ob).parents(".form-group").find(".select_photo").val(evt.data.resizedUrl);
			} );
		}
	} ); 
}
function removeImgLink(ob){// remove link photo 
	$(ob).parents(".photo_content").find(".select_photo").attr("value","");//get id of input with class 'select_photo' when click button select
	$(ob).parents(".photo_content").find(".select_photo").val("");//get id of input with class 'select_photo' when click button select
	$(ob).parents(".photo_content").find(".display_image").hide('slow');
} 

function addMorePhoto(ob){
	let cloneItem = $(ob).parents(".content_photo").find(".item_photo").first().clone();
	cloneItem.find('.display_image').hide();
	cloneItem.find('input').val('');
	$(cloneItem).hide().appendTo(".list_photo").fadeIn(1000);
}
function removeImgLinkPosition(ob){// remove photo link & position
	$(ob).parents(".item_photo").find(".select_photo").attr("value","");//remove img link
	$(ob).parents(".item_photo").find(".select_photo").val("");
	$(ob).parents(".item_photo").find(".img_position").attr("value","");//remove img position
	$(ob).parents(".item_photo").find(".img_position").val("");
	$(ob).parents(".item_photo").find(".display_image").hide('slow');
} 
function toggleAllChecksPrefix(formName, prefix)
{
	n = "all";
    if (prefix)
    {
        n = prefix + '_' + n;
    }
    i = 0;
    e = document.getElementById(n);
    s = e.checked;
    f = document.getElementById(formName);
    while (e = f.elements[i])
    {
        if (e.type == "checkbox" && e.id != n)
        {
            if (e.id.indexOf(prefix) != -1)
            {
                e.checked = s;
            }
        }
        i++;
    }
}
function toggleAllChecks(ob){
	// console.log("checked");

	if($(ob).prop("checked")){
		$(ob).parents('.form-group').find("input.checkbox-input").attr("checked", "checked");
		$(ob).parents('.form-group').find("input.checkbox-input").prop("checked",true);
	}else{
		$(ob).parents('.form-group').find("input.checkbox-input").removeAttr("checked");
		$(ob).parents('.form-group').find("input.checkbox-input").prop("checked",false);
	}
}
$("#permissionForm input").on("ifChecked", function () {
    if($(this).parent().find("input.all").length){// if current checkbox isset class all => check all input in this group
		$(this).parents('.form-group').find(".icheckbox_flat-green").addClass("checked");
        $(this).parents('.form-group').find("input.checkbox-input").attr("checked", "checked");
        $(this).parents('.form-group').find("input.checkbox-input").prop("checked", true);
	}
});
$("#permissionForm input").on("ifUnchecked", function () {
    if($(this).parent().find("input.all").length){// if current checkbox isset class all => check all input in this group
		$(this).parents('.form-group').find(".icheckbox_flat-green").removeClass("checked");
        $(this).parents('.form-group').find("input.checkbox-input").removeAttr("checked");
        $(this).parents('.form-group').find("input.checkbox-input").prop("checked", false);
	}
});
// $("#check-all-input").click(function(){
// 	if($(this).prop("checked")){
// 		$("input.checkbox-input").attr("checked", "checked");
// 		$("input.checkbox-input").prop("checked",true);
// 	}else{
// 		$("input.checkbox-input").removeAttr("checked");
// 		$("input.checkbox-input").prop("checked",false);
// 	}
// })

// $(".bulk_action input#category_all").on("ifChecked", function () {
// 	console.log("checked 1");
// })

// var checkState = "";
// function countChecked() {
//     "all" === checkState && $(".bulk_action input.checkbox-input").iCheck("check"), "none" === checkState && $(".bulk_action input.checkbox-input").iCheck("uncheck");
//     var e = $(".bulk_action input.checkbox-input:checked").length;
//     e ? ($(".column-title").hide(), $(".bulk-actions").show(), $(".action-cnt").html(e + " Records Selected")) : ($(".column-title").show(), $(".bulk-actions").hide());
// }
// $(".bulk_action input#check-all").on("ifChecked", function () {
// 	(checkState = "all"), countChecked();
// })
$('#registerForm').on('submit', function(e) {
    e.preventDefault();

    // let timerInterval = 10000;
    Swal.fire({
    title: 'Please wait a moment',
    html: '',
    timer: 20000,
    timerProgressBar: true,
    didOpen: () => {
        Swal.showLoading();
    },
    // willClose: () => {
    //     clearInterval(timerInterval);
    // }
    }).then((result) => {
    // if (result.dismiss === Swal.DismissReason.timer) {
    //     console.log('I was closed by the timer')
    // }
    })
    // setTimeout(function(){
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
            },
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : new FormData($('#registerForm')[0]),
            processData:false,
            type : 'POST',
            dataType : 'json',
            contentType: false,
            beforeSend:function(){
                $(document).find('p.text-danger').text('');
            },
            success : function(data){
                if(data.status == '1'){
                    $('#registerForm')[0].reset();
                    // window.location.reload();
                    setTimeout(function(){ window.location.href = '/control/dashboard'; }, 3000);
                }else{
                    $.each(data.error, function(prefix, val){
                        $('p.'+ prefix +'_error').text(val[0]);
                    });
                } 
                Swal.fire({
                    position: 'center',
                    icon: data.icon,
                    title: data.msg,
                    showConfirmButton: false,
                }); 
            }
        });
    // }, 2000);
});
function showPassword(ob){
    var inputPassword = $(ob).parents('.form-group').find('input.password');
    if (inputPassword.attr("type") == "password") {
        inputPassword.attr("type", "text");
    } else {
        inputPassword.attr("type", "password");
    }
}
$('#api_token_form').on('submit', function(e) {
    e.preventDefault();
    Swal.fire({
    title: 'Please wait a moment',
    html: '',
    timer: 5000,
    timerProgressBar: true,
    didOpen: () => {
        Swal.showLoading();
    },
    })
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        url : $(this).attr('action'),
        method : $(this).attr('method'),
        data : new FormData($('#api_token_form')[0]),
        processData:false,
        type : 'POST',
        dataType : 'json',
        contentType: false,
        success : function(data){
            if(data.status == '1'){
                $('#api_token_form').find('input.api_token').val(data.token);
            }
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.msg,
                showConfirmButton: false,
            }); 
        }
    });
    // $('form').submit();
});