$('#comment_article').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        url : $(this).attr('action'),
        method : $(this).attr('method'),
        data : new FormData($('#comment_article')[0]),
        processData:false,
        type : 'POST',
        dataType : 'json',
        contentType: false,
        beforeSend:function(){
            $(document).find('p.text-danger').text('');
        },
        success : function(data){
            if(data.status == '0'){
                $.each(data.error, function(prefix, val){
                    $('p.'+ prefix +'_error').text(val[0]);
                });
            }else{
                $('#comment_article')[0].reset();
            } 
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.msg,
                showConfirmButton: false,
            });
        }
    });
});
$('#contact_form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        url : $(this).attr('action'),
        method : $(this).attr('method'),
        data : new FormData($('#contact_form')[0]),
        processData:false,
        type : 'POST',
        dataType : 'json',
        contentType: false,
        beforeSend:function(){
            $(document).find('p.text-danger').text('');
        },
        success : function(data){
            if(data.status == '0'){
                $.each(data.error, function(prefix, val){
                    $('p.'+ prefix +'_error').text(val[0]);
                });
            }else{
                $('#contact_form')[0].reset();
            } 
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.msg,
                showConfirmButton: false,
            });
        }
    });
});
$('#newsletter_form').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        url : $(this).attr('action'),
        method : $(this).attr('method'),
        data : new FormData($('#newsletter_form')[0]),
        processData:false,
        type : 'POST',
        dataType : 'json',
        contentType: false,
        beforeSend:function(){
            $(document).find('p.text-danger').text('');
        }, 
        success : function(data){
            if(data.status == '0'){
                $.each(data.error, function(prefix, val){
                    $('p.'+ prefix +'_error').text(val[0]);
                });
            }else{
                $('#newsletter_form')[0].reset();
            } 
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.msg,
                showConfirmButton: false,
            });
        }
    });
});
$('#book_a_seat').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') 
        },
        url : $(this).attr('action'),
        method : $(this).attr('method'),
        data : new FormData($('#book_a_seat')[0]),
        processData:false,
        type : 'POST',
        dataType : 'json',
        contentType: false,
        beforeSend:function(){
            $(document).find('p.text-danger').text('');
        }, 
        success : function(data){
            if(data.status == '0'){
                $.each(data.error, function(prefix, val){
                    $('p.'+ prefix +'_error').text(val[0]);
                });
            }else{
                $('#book_a_seat')[0].reset();
            } 
            Swal.fire({
                position: 'center',
                icon: data.icon,
                title: data.msg,
                showConfirmButton: false,
            });
        }
    });
});