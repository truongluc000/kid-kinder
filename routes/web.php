<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdsController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\StaticController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\LibraryController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\AdsCategoriesController;
use App\Http\Controllers\ConfigGeneralController;
use App\Http\Controllers\ArticleCategoriesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [HomeController::class, 'index']);
Route::post('/saveComment', [HomeController::class, 'saveComment'])->name('home.saveComment');
Route::post('/saveContact', [HomeController::class, 'saveContact'])->name('home.saveContact');
Route::post('/saveNewsletter', [HomeController::class, 'saveNewsletter'])->name('home.saveNewsletter');
Route::post('/bookASeat', [HomeController::class, 'bookASeat'])->name('home.bookASeat');
Route::get('/novel.html', [HomeController::class, 'novel'])->name('home.novel');
Route::get('/{slug}.html', [HomeController::class, 'page'])->where('slug', '[a-z-_]+')->name('home.page');
Route::get('/{category}/{slug}.html', [HomeController::class, 'article'])->where(['category' => '[a-z-_]+', 'slug' => '[a-z0-9-_]+'])->name('home.article');

/*    
ADMIN page routes
*/
$adminRedirect = config('constants.adminLink');// admin url
Route::group(['prefix' => $adminRedirect], function () {
    // base admin route
    Route::get('/', [AuthController::class, 'index'])->middleware('auth');
    Route::get('/dashboard', [AuthController::class, 'index'])->name('auth.index')->middleware('auth');
    Route::get('/login', [AuthController::class, 'login'])->name('auth.login');
    Route::post('/postLogin', [AuthController::class, 'postLogin'])->name('auth.postLogin');
    Route::post('/postRegister', [AuthController::class, 'postRegister'])->name('auth.postRegister');
    // Route::get('/loadingpage', [AuthController::class, 'loadingpage'])->name('auth.loadingpage');
    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');

    Route::group(['prefix' => 'account','middleware' => 'auth'], function () {
        Route::get('/', [AuthController::class, 'show'])->name('account.show');
        Route::put('/edit', [AuthController::class, 'edit'])->name('account.edit');
        Route::post('/token', [AuthController::class, 'token'])->name('account.token');

    });
    // User manager with middleware auth. Can use resource, but here we set permission for each route of the routes
    //Route::resource('user',UserController::class)->names('users')->name('user.show')->middleware('role:admin');
    Route::group(['prefix' => 'user','middleware' => ['role:admin']], function () {
        Route::get('/', [UserController::class, 'index'])->name('user.index');
        Route::get('/trackings', [UserController::class, 'trackings'])->name('user.trackings');
        Route::get('/trackings/search', [SearchController::class, 'search']);
        Route::get('/create', [UserController::class, 'create'])->name('user.create'); 
        Route::post('/store', [UserController::class, 'store'])->name('user.store');
        Route::get('/show/{id}', [UserController::class, 'show'])->where('id','[0-9]+')->name('user.show');
        Route::put('/edit/{id}', [UserController::class, 'edit'])->where('id','[0-9]+')->name('user.edit');
        Route::get('/permission/{id}', [UserController::class, 'permission'])->where('id','[0-9]+')->name('user.permission');
        Route::put('/permissionUpdate/{id}', [UserController::class, 'permissionUpdate'])->where('id','[0-9]+')->name('user.permissionUpdate');
        Route::get('/changeStatus/{status}/{id}', [UserController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('user.changeStatus');// icon update status for each item
        Route::post('/update', [UserController::class, 'update'])->name('user.update');// button update list for checked item
        Route::get('/clean', [UserController::class, 'clean'])->name('user.clean');
        Route::delete('/destroy', [UserController::class, 'destroy'])->name('user.destroy');
        Route::get('/search', [SearchController::class, 'search']);
    });
    // Menu manager with middleware auth
    Route::group(['prefix' => 'menu','middleware' => 'auth'], function () {
        Route::get('/', [MenuController::class, 'index'])->name('menu.index')->middleware('role_or_permission:admin|view_menu');
        Route::get('/create', [MenuController::class, 'create'])->name('menu.create')->middleware('role_or_permission:admin|add_menu');
        Route::post('/store', [MenuController::class, 'store'])->name('menu.store')->middleware('role_or_permission:admin|add_menu');
        Route::get('/show/{id}', [MenuController::class, 'show'])->where('id','[0-9]+')->name('menu.show')->middleware('role_or_permission:admin|edit_menu');
        Route::put('/edit/{id}', [MenuController::class, 'edit'])->where('id','[0-9]+')->name('menu.edit')->middleware('role_or_permission:admin|edit_menu');
        Route::get('/changeStatus/{status}/{id}', [MenuController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('menu.changeStatus')->middleware('role_or_permission:admin|edit_menu');// icon update status for each item
        Route::post('/update', [MenuController::class, 'update'])->name('menu.update')->middleware('role_or_permission:admin|edit_menu');// button update list for checked item
        Route::get('/clean', [MenuController::class, 'clean'])->name('menu.clean')->middleware('role_or_permission:admin|delete_menu');
        Route::delete('/destroy', [MenuController::class, 'destroy'])->name('menu.destroy')->middleware('role_or_permission:admin|delete_menu');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_menu');
    });
    // Classes manager with middleware auth
    Route::group(['prefix' => 'classes','middleware' => 'auth'], function () {
        Route::get('/', [ClassesController::class, 'index'])->name('classes.index')->middleware('role_or_permission:admin|view_classes');
        Route::get('/create', [ClassesController::class, 'create'])->name('classes.create')->middleware('role_or_permission:admin|add_classes');
        Route::post('/store', [ClassesController::class, 'store'])->name('classes.store')->middleware('role_or_permission:admin|add_classes');
        Route::get('/show/{id}', [ClassesController::class, 'show'])->where('id','[0-9]+')->name('classes.show')->middleware('role_or_permission:admin|edit_classes');
        Route::put('/edit/{id}', [ClassesController::class, 'edit'])->where('id','[0-9]+')->name('classes.edit')->middleware('role_or_permission:admin|edit_classes');
        Route::get('/changeStatus/{status}/{id}', [ClassesController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('classes.changeStatus')->middleware('role_or_permission:admin|edit_classes');// icon update status for each item
        Route::post('/update', [ClassesController::class, 'update'])->name('classes.update')->middleware('role_or_permission:admin|edit_classes');// button update list for checked item
        Route::get('/clean', [ClassesController::class, 'clean'])->name('classes.clean')->middleware('role_or_permission:admin|delete_classes');
        Route::delete('/destroy', [ClassesController::class, 'destroy'])->name('classes.destroy')->middleware('role_or_permission:admin|delete_classes');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_classes');
    });
    // Teachers manager with middleware auth
    Route::group(['prefix' => 'teacher','middleware' => 'auth'], function () {
        Route::get('/', [TeacherController::class, 'index'])->name('teacher.index')->middleware('role_or_permission:admin|view_teacher');
        Route::get('/create', [TeacherController::class, 'create'])->name('teacher.create')->middleware('role_or_permission:admin|add_teacher');
        Route::post('/store', [TeacherController::class, 'store'])->name('teacher.store')->middleware('role_or_permission:admin|add_teacher');
        Route::get('/show/{id}', [TeacherController::class, 'show'])->where('id','[0-9]+')->name('teacher.show')->middleware('role_or_permission:admin|edit_teacher');
        Route::put('/edit/{id}', [TeacherController::class, 'edit'])->where('id','[0-9]+')->name('teacher.edit')->middleware('role_or_permission:admin|edit_teacher');
        Route::get('/changeStatus/{status}/{id}', [TeacherController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('teacher.changeStatus')->middleware('role_or_permission:admin|edit_teacher');// icon update status for each item
        Route::post('/update', [TeacherController::class, 'update'])->name('teacher.update')->middleware('role_or_permission:admin|edit_teacher');// button update list for checked item
        Route::get('/clean', [TeacherController::class, 'clean'])->name('teacher.clean')->middleware('role_or_permission:admin|delete_teacher');
        Route::delete('/destroy', [TeacherController::class, 'destroy'])->name('teacher.destroy')->middleware('role_or_permission:admin|delete_teacher');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_teacher');
    });
    // Ads manager with middleware auth
    Route::group(['prefix' => 'ads','middleware' => 'auth'], function () {
        Route::get('/', [AdsController::class, 'index'])->name('ads.index')->middleware('role_or_permission:admin|view_ads');
        Route::get('/create', [AdsController::class, 'create'])->name('ads.create')->middleware('role_or_permission:admin|add_ads');
        Route::post('/store', [AdsController::class, 'store'])->name('ads.store')->middleware('role_or_permission:admin|add_ads');
        Route::get('/show/{id}', [AdsController::class, 'show'])->where('id','[0-9]+')->name('ads.show')->middleware('role_or_permission:admin|edit_ads');
        Route::put('/edit/{id}', [AdsController::class, 'edit'])->where('id','[0-9]+')->name('ads.edit')->middleware('role_or_permission:admin|edit_ads');
        Route::get('/changeStatus/{status}/{id}', [AdsController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('ads.changeStatus')->middleware('role_or_permission:admin|edit_ads');// icon update status for each item
        Route::post('/update', [AdsController::class, 'update'])->name('ads.update')->middleware('role_or_permission:admin|edit_ads');// button update list for checked item
        Route::get('/clean', [AdsController::class, 'clean'])->name('ads.clean')->middleware('role_or_permission:admin|delete_ads');
        Route::delete('/destroy', [AdsController::class, 'destroy'])->name('ads.destroy')->middleware('role_or_permission:admin|delete_ads');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_ads');
    });
    // Ads categories manager with middleware auth
    Route::group(['prefix' => 'adscategories','middleware' => 'auth'], function () {
        Route::get('/', [AdsCategoriesController::class, 'index'])->name('adscategories.index')->middleware('role_or_permission:admin|view_ads_category');
        Route::get('/create', [AdsCategoriesController::class, 'create'])->name('adscategories.create')->middleware('role_or_permission:admin|add_ads_category');
        Route::post('/store', [AdsCategoriesController::class, 'store'])->name('adscategories.store')->middleware('role_or_permission:admin|add_ads_category');
        Route::get('/show/{id}', [AdsCategoriesController::class, 'show'])->where('id','[0-9]+')->name('adscategories.show')->middleware('role_or_permission:admin|edit_ads_category');
        Route::put('/edit/{id}', [AdsCategoriesController::class, 'edit'])->where('id','[0-9]+')->name('adscategories.edit')->middleware('role_or_permission:admin|edit_ads_category');
        Route::get('/changeStatus/{status}/{id}', [AdsCategoriesController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('adscategories.changeStatus')->middleware('role_or_permission:admin|edit_ads_category');// icon update status for each item
        Route::post('/update', [AdsCategoriesController::class, 'update'])->name('adscategories.update')->middleware('role_or_permission:admin|edit_ads_category');// button update list for checked item
        Route::get('/clean', [AdsCategoriesController::class, 'clean'])->name('adscategories.clean')->middleware('role_or_permission:admin|delete_ads_category');
        Route::delete('/destroy', [AdsCategoriesController::class, 'destroy'])->name('adscategories.destroy')->middleware('role_or_permission:admin|delete_ads_category');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_ads_category');
    });
    // Gallery manager with middleware auth
    Route::group(['prefix' => 'gallery','middleware' => 'auth'], function () {
        Route::get('/', [GalleryController::class, 'index'])->name('gallery.index')->middleware('role_or_permission:admin|view_gallery');
        Route::get('/create', [GalleryController::class, 'create'])->name('gallery.create')->middleware('role_or_permission:admin|add_gallery');
        Route::post('/store', [GalleryController::class, 'store'])->name('gallery.store')->middleware('role_or_permission:admin|add_gallery');
        Route::get('/show/{id}', [GalleryController::class, 'show'])->where('id','[0-9]+')->name('gallery.show')->middleware('role_or_permission:admin|edit_gallery');
        Route::put('/edit/{id}', [GalleryController::class, 'edit'])->where('id','[0-9]+')->name('gallery.edit')->middleware('role_or_permission:admin|edit_gallery');
        Route::get('/changeStatus/{status}/{id}', [GalleryController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('gallery.changeStatus')->middleware('role_or_permission:admin|edit_gallery');// icon update status for each item
        Route::post('/update', [GalleryController::class, 'update'])->name('gallery.update')->middleware('role_or_permission:admin|edit_gallery');// button update list for checked item
        Route::get('/clean', [GalleryController::class, 'clean'])->name('gallery.clean')->middleware('role_or_permission:admin|delete_gallery');
        Route::delete('/destroy', [GalleryController::class, 'destroy'])->name('gallery.destroy')->middleware('role_or_permission:admin|delete_gallery');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_gallery');
    });
    // Library manager with middleware auth
    Route::group(['prefix' => 'library','middleware' => ['role_or_permission:admin|view_library']], function () {// middleware role here to redirect 403 instead of login
        Route::get('/', [LibraryController::class, 'index'])->name('library.index');
    });
    // Article manager with middleware auth
    Route::group(['prefix' => 'article','middleware' => 'auth'], function () {
        Route::get('/', [ArticleController::class, 'index'])->name('article.index')->middleware('role_or_permission:admin|view_article');
        Route::get('/create', [ArticleController::class, 'create'])->name('article.create')->middleware('role_or_permission:admin|add_article');
        Route::post('/store', [ArticleController::class, 'store'])->name('article.store')->middleware('role_or_permission:admin|add_article');
        Route::get('/show/{id}', [ArticleController::class, 'show'])->where('id','[0-9]+')->name('article.show')->middleware('role_or_permission:admin|edit_article');
        Route::put('/edit/{id}', [ArticleController::class, 'edit'])->where('id','[0-9]+')->name('article.edit')->middleware('role_or_permission:admin|edit_article');
        Route::get('/changeStatus/{status}/{id}', [ArticleController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('article.changeStatus')->middleware('role_or_permission:admin|edit_article');// icon update status for each item
        Route::post('/update', [ArticleController::class, 'update'])->name('article.update')->middleware('role_or_permission:admin|edit_article');// button update list for checked item
        Route::get('/clean', [ArticleController::class, 'clean'])->name('article.clean')->middleware('role_or_permission:admin|delete_article');
        Route::delete('/destroy', [ArticleController::class, 'destroy'])->name('article.destroy')->middleware('role_or_permission:admin|delete_article');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_article');
    });
    // Article Category manager with middleware auth
    Route::group(['prefix' => 'articlecategories','middleware' => 'auth'], function () {
        Route::get('/', [ArticleCategoriesController::class, 'index'])->name('articlecategories.index')->middleware('role_or_permission:admin|view_article_category');
        Route::get('/create', [ArticleCategoriesController::class, 'create'])->name('articlecategories.create')->middleware('role_or_permission:admin|add_article_category');
        Route::post('/store', [ArticleCategoriesController::class, 'store'])->name('articlecategories.store')->middleware('role_or_permission:admin|add_article_category');
        Route::get('/show/{id}', [ArticleCategoriesController::class, 'show'])->where('id','[0-9]+')->name('articlecategories.show')->middleware('role_or_permission:admin|edit_article_category');
        Route::put('/edit/{id}', [ArticleCategoriesController::class, 'edit'])->where('id','[0-9]+')->name('articlecategories.edit')->middleware('role_or_permission:admin|edit_article_category');
        Route::get('/changeStatus/{status}/{id}', [ArticleCategoriesController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('articlecategories.changeStatus')->middleware('role_or_permission:admin|edit_article_category');// icon update status for each item
        Route::post('/update', [ArticleCategoriesController::class, 'update'])->name('articlecategories.update')->middleware('role_or_permission:admin|edit_article_category');// button update list for checked item
        Route::get('/clean', [ArticleCategoriesController::class, 'clean'])->name('articlecategories.clean')->middleware('role_or_permission:admin|delete_article_category');
        Route::delete('/destroy', [ArticleCategoriesController::class, 'destroy'])->name('articlecategories.destroy')->middleware('role_or_permission:admin|delete_article_category');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_article_category');
    });
    // Static manager with middleware auth
    Route::group(['prefix' => 'static','middleware' => 'auth'], function () {
        Route::get('/', [StaticController::class, 'index'])->name('static.index')->middleware('role_or_permission:admin|view_static');
        Route::get('/create', [StaticController::class, 'create'])->name('static.create')->middleware('role_or_permission:admin|add_static');
        Route::post('/store', [StaticController::class, 'store'])->name('static.store')->middleware('role_or_permission:admin|add_static');
        Route::get('/show/{id}', [StaticController::class, 'show'])->where('id','[0-9]+')->name('static.show')->middleware('role_or_permission:admin|edit_static');
        Route::put('/edit/{id}', [StaticController::class, 'edit'])->where('id','[0-9]+')->name('static.edit')->middleware('role_or_permission:admin|edit_static');
        Route::get('/changeStatus/{status}/{id}', [StaticController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('static.changeStatus')->middleware('role_or_permission:admin|edit_static');// icon update status for each item
        Route::post('/update', [StaticController::class, 'update'])->name('static.update')->middleware('role_or_permission:admin|edit_static');// button update list for checked item
        Route::get('/clean', [StaticController::class, 'clean'])->name('static.clean')->middleware('role_or_permission:admin|delete_static');
        Route::delete('/destroy', [StaticController::class, 'destroy'])->name('static.destroy')->middleware('role_or_permission:admin|delete_static');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_static');
    });
    // Comment manager with middleware auth
    Route::group(['prefix' => 'comment','middleware' => 'auth'], function () {
        Route::get('/', [CommentsController::class, 'index'])->name('comment.index')->middleware('role_or_permission:admin|view_comment');
        Route::get('/changeStatus/{status}/{id}', [CommentsController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('comment.changeStatus')->middleware('role_or_permission:admin|edit_comment');// icon update status for each item
        Route::post('/update', [CommentsController::class, 'update'])->name('comment.update')->middleware('role_or_permission:admin|edit_comment');// button update list for checked item
        Route::get('/clean', [CommentsController::class, 'clean'])->name('comment.clean')->middleware('role_or_permission:admin|delete_comment');
        Route::delete('/destroy', [CommentsController::class, 'destroy'])->name('comment.destroy')->middleware('role_or_permission:admin|delete_comment');
        Route::get('/search', [SearchController::class, 'search'])->middleware('role_or_permission:admin|view_comment');
    });
    // Contact manager with middleware auth
    Route::group(['prefix' => 'contact','middleware' => 'auth'], function () {
        Route::get('/', [ContactController::class, 'index'])->name('contact.index')->middleware('role_or_permission:admin|view_contact');
        Route::get('/changeStatus/{status}/{id}', [ContactController::class, 'changeStatus'])->where(['status' => '[0-9]+','id' => '[0-9]+'])->name('contact.changeStatus')->middleware('role_or_permission:admin|edit_contact');// icon update status for each item
        Route::post('/update', [ContactController::class, 'update'])->name('contact.update')->middleware('role_or_permission:admin|edit_contact');// button update list for checked item
        Route::get('/clean', [ContactController::class, 'clean'])->name('contact.clean')->middleware('role_or_permission:admin|delete_contact');
        Route::delete('/destroy', [ContactController::class, 'destroy'])->name('contact.destroy')->middleware('role_or_permission:admin|delete_contact');
        Route::get('/search', [Sentactntroller::class, 'search'])->middleware('role_or_permission:admin|view_contact');
    });
    // System config with middleware auth 
    Route::group(['prefix' => 'config','middleware' => ['role:admin']], function () {
        Route::get('/', [ConfigGeneralController::class, 'show'])->name('config.show');
        Route::put('/edit', [ConfigGeneralController::class, 'edit'])->name('config.edit');
    });
});
//ckfinder route from \vendor\ckfinder\ckfinder-laravel-package\src
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
->name('ckfinder_connector');
Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
->name('ckfinder_browser');
// Default language from config file 
App::setLocale(config('constants.defaultLang'));
