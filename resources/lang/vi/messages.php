<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for view & controller
    |
    */

    'Welcome' => 'Xin chào, :Name',
    'goodbye' => 'Tạm biệt, :Name',
    'update' => 'Cập nhật',
    'site_name' => 'Kid Kinder',
    'update_msg' => 'Cập nhật',
    'add' => 'Thêm mới',
    'enable_msg' => 'Kích hoạt',
    'disable_msg' => 'Vô hiệu',
    'delete_msg' => 'Xóa',
    'list' => 'Danh sách',
    'group' => 'Nhóm',
    'search' => 'Tìm kiếm',
    'no' => 'STT',
    'id' => 'Id',
    'name' => 'Tên',
    'email' => 'Email',
    'created' => 'Ngày tạo',
    'updated' => 'Cập nhật',
    'fullname' => 'Họ & tên',
    'action' => 'hành động',
    'status' => 'Trạng thái',
    'password' => 'Mật khẩu',
    'confirm_password' => 'Xác nhận mật khẩu',
    'password_confirmation' => 'Xác nhận mật khẩu',
    'login_form' => 'Đăng nhập',
    'login' => 'Đăng nhập',
    'logout' => 'Đăng xuất',
    'forgot_password' => 'Quên mật khẩu',
    'register' => 'Đăng ký',
    'create_account' => 'Tạo tài khoản',
    'submit' => 'Gửi',
    'list_user' => 'Danh sách người dùng',
    'add_new' => 'Thêm mới',
    'view_home_page' => 'Xem trang chủ',
    'save' => 'Lưu',
    'clear' => 'Nhập lại',
    'reset' => 'Nhập lại',
    'cancel' => 'Hủy',
    'username' => 'Tên đăng nhập',
    'email_exist' => 'Email đã tồn tại',
    'add_new_success' => 'Thêm mới thành công',
    'failed' => 'Failed',
    'user_not_exist' => 'User is not exists',
    'update_success' => 'Update item successfully',
    'update_success_id' => 'Update item with ID :id successfully',
    'login_success' => 'Login successfully',
    'logout_success' => 'Logout successfully',
    'user_password_incorrect' => 'Username or password is incorrect',
    'failed_registration' => 'Registration failed',
    'no_item_selected' => 'No item selected',
    'no_result' => 'No results found',
];
