@extends('home.error')
@section('content')
    <!-- Error Start -->
    <h3 class="display-3 font-weight-bold text-white">{{ $error }}</h3>
    <!-- Error End -->
@endsection 