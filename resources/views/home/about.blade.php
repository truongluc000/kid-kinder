@if ($aboutItem ?? '')
<div class="container-fluid py-5">
    <div class="container">
        <div class="row align-items-center">
            @if ($aboutItem->getProperty('avatar'))
            <div class="col-lg-5">
                <img class="img-fluid rounded mb-5 mb-lg-0" src="{{ $aboutItem->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="">
            </div>
            @endif
            <div class="col-lg-7">
                <p class="section-title pr-5"><span class="pr-2">{{ $aboutItem->name }}</span></p>
                <h1 class="mb-4">{{ $aboutItem->getProperty('keywords') }}</h1>
                <p>{{ $aboutItem->getProperty('sapo') }}</p>
                <div class="row pt-2 pb-4">{!! $aboutItem->getProperty('details') !!}</div>
                <a href="/{{ $aboutItem->slug }}.html" class="btn btn-primary mt-2 py-2 px-4">{{__('messages.learn_more')}}</a>
            </div>
        </div>
    </div>
</div>
@endif