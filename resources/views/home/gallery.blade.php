@if ($galleryItem ?? '')
<div class="container-fluid pt-5 pb-3">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">{{ $galleryItem->name }}</span></p>
            <h1 class="mb-4">{{ $galleryItem->getProperty('keywords') }}</h1>
        </div>
        @if ($listGallery ?? '')
        <div class="row">
            <div class="col-12 text-center mb-2">
                <ul class="list-inline mb-4" id="portfolio-flters">
                    <li class="btn btn-outline-primary m-1 active"  data-filter="*">{{__('messages.all')}}</li>
                    @foreach ($listGallery as $itemGallery)
                    <li class="btn btn-outline-primary m-1" data-filter=".class_gallery_{{ $itemGallery->id }}">{{ $itemGallery->name }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row portfolio-container">
            @foreach ($listGallery as $itemGallery)
                @if ($itemGallery->getProperty('arrayPhoto') ?? '')
                    @foreach ($itemGallery->getProperty('arrayPhoto') as $itemPhotoG)
                    <div class="col-lg-4 col-md-6 mb-4 portfolio-item class_gallery_{{ $itemGallery->id }}">
                        <div class="position-relative overflow-hidden mb-2">
                            <img class="img-fluid w-100" src="{{ $itemPhotoG['photo'] }}" alt="">
                            <div class="portfolio-btn bg-primary d-flex align-items-center justify-content-center">
                                <a href="{{ $itemPhotoG['photo'] }}" data-lightbox="portfolio">
                                    <i class="fa fa-plus text-white" style="font-size: 60px;"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            @endforeach
        </div>
        @endif
    </div>
</div>
@endif