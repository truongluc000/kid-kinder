<!DOCTYPE html>
<html lang="en">
    @include('home.head') 
    <!-- Navbar Start -->
    @include('home.menu')  
    <!-- Navbar End -->

    @yield('content')
    
    <!-- Footer Start -->
    @include('home.footer') 
    <!-- Footer End -->
</html> 