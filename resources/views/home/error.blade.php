<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <title>Page Error</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Nunito&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="{{ url('/') }}/frontend/lib/flaticon/font/flaticon.css" rel="stylesheet">
        <link href="{{ url('/') }}/frontend/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="{{ url('/') }}/frontend/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
        <link href="{{ url('/') }}/frontend/css/style.css" rel="stylesheet">
    </head>
    <body>  
        <div class="container-fluid bg-light position-relative shadow">
            <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0 px-lg-5">
                <a href="/" class="navbar-brand font-weight-bold text-secondary" style="font-size: 50px;">
                    @if ($configData['avatar'] ?? '')<img src="{{ $configData['avatar'] }}?v={{ $configData['version_home'] }}">@endif
                    <span class="text-primary">@if ($configData['name'] ?? '') {{ $configData['name'] }} @endif</span>
                </a>
            </nav> 
        </div>
        <div class="container-fluid bg-primary">
            <div class="d-flex flex-column align-items-center justify-content-center" style="min-height: 400px;padding: 34vh 0px;">
                @yield('content')
            </div>
        </div>
    <div class="container-fluid bg-secondary text-white py-5 px-sm-3 px-md-5"  style="position: fixed; bottom: 0;">
        
        @if ($configData['copyright'] ?? '')
        <div class="container-fluid pt-5" style="border-top: 1px solid rgba(23, 162, 184, .2);;">
            <p class="m-0 text-center text-white">
                {!! $configData['copyright'] !!}
            </p>
        </div>
        @endif
    </div>
    <a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>
    <script src="{{ url('/') }}/frontend/js/jquery-3.6.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="{{ url('/') }}/frontend/lib/easing/easing.min.js"></script>
    <script src="{{ url('/') }}/frontend/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="{{ url('/') }}/frontend/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="{{ url('/') }}/frontend/lib/lightbox/js/lightbox.min.js"></script>
    <script src="{{ url('/') }}/frontend/mail/jqBootstrapValidation.min.js"></script>
    </body>
</html> 