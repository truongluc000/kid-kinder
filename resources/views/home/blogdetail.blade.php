@if ($objectItem ?? '')
<div class="container py-5">
    <div class="row pt-5">
        <div class="col-lg-8">
            <div class="d-flex flex-column text-left mb-3">
                <p class="section-title pr-5"><span class="pr-2">Blog Detail Page</span></p>
                <h1 class="mb-3">{{ $objectItem->name }}</h1> 
                <div class="d-flex">
                    <p class="mr-3"><i class="fa fa-user text-primary"></i> {{$objectItem->author->name}}</p>
                    <p class="mr-3"><i class="fa fa-folder text-primary"></i> {{$objectItem->categories->name}}</p>
                    <p class="mr-3"><i class="fa fa-comments text-primary"></i> {{ count($objectItem->listComments) }}</p>
                </div>
            </div>
            <div class="mb-5 article-detail-content">
                {!! $objectItem->getProperty('details') !!}
            </div>
            @if ($otherPost ?? '')
            <!-- Related Post -->
            <div class="mb-5 mx-n3">
                <h2 class="mb-4 ml-3">{{__('messages.related_post')}}</h2>
                <div class="owl-carousel post-carousel position-relative">
                    @foreach ($otherPost as $otherPostItem)
                    <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mx-3">
                        <img class="img-fluid" src="{{ $otherPostItem->getProperty('avatar') }}?v={{ $configData['version_home'] }}" style="width: 80px; height: 80px;">
                        <div class="pl-3">
                            <h5 class="">{{ $otherPostItem->name }}</h5>
                            <div class="d-flex">
                                @if ($otherPostItem->author ?? '')<small class="mr-3"><i class="fa fa-user text-primary"></i> {{$otherPostItem->author->name}}</small>@endif
                                @if ($otherPostItem->categories ?? '')<small class="mr-3"><i class="fa fa-folder text-primary"></i> {{$otherPostItem->categories->name}}</small>@endif
                                @if ($otherPostItem->listComments ?? '')<small class="mr-3"><i class="fa fa-comments text-primary"></i> {{ count($otherPostItem->listComments) }}</small>@endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif

            <!-- Comment List -->
            @if ($listComments ?? '')
            <div class="mb-5">
                <h2 class="mb-4">{{ count($objectItem->listComments) }} {{__('messages.comments')}}</h2>
                @foreach ($listComments as $commentItem)
                <div class="media mb-4">
                    <img src="{{ $configData['favicon'] }}?v={{ $configData['version_home'] }}" alt="Image" class="img-fluid rounded-circle mr-3 mt-1" style="width: 45px;">
                    <div class="media-body">
                        <h6>{{ $commentItem->name }} <small><i>{{ $commentItem->getDateCreated() }}</i></small></h6>
                        <p>{{ $commentItem->getProperty('message') }}</p>
                        {{-- <button class="btn btn-sm btn-light">Reply</button> --}}
                    </div>
                </div>
                @endforeach
                {{-- <div class="media mb-4">
                    <img src="../frontend/img/user.jpg" alt="Image" class="img-fluid rounded-circle mr-3 mt-1" style="width: 45px;">
                    <div class="media-body">
                        <h6>John Doe <small><i>01 Jan 2045 at 12:00pm</i></small></h6>
                        <p>Diam amet duo labore stet elitr ea clita ipsum, tempor labore accusam ipsum et no at. Kasd diam tempor rebum magna dolores sed sed eirmod ipsum. Gubergren clita aliquyam consetetur sadipscing, at tempor amet ipsum diam tempor consetetur at sit.</p>
                        <button class="btn btn-sm btn-light">Reply</button>
                        <div class="media mt-4">
                            <img src="../frontend/img/user.jpg" alt="Image" class="img-fluid rounded-circle mr-3 mt-1" style="width: 45px;">
                            <div class="media-body">
                                <h6>John Doe <small><i>01 Jan 2045 at 12:00pm</i></small></h6>
                                <p>Diam amet duo labore stet elitr ea clita ipsum, tempor labore accusam ipsum et no at. Kasd diam tempor rebum magna dolores sed sed eirmod ipsum. Gubergren clita aliquyam consetetur, at tempor amet ipsum diam tempor at sit.</p>
                                <button class="btn btn-sm btn-light">Reply</button>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            @endif

            <!-- Comment Form -->
            <div class="bg-light p-5">
                <h2 class="mb-4">Leave a comment</h2> 
                <form action="{{ route('home.saveComment') }}" method="post" role="form" id="comment_article">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name *</label>
                        <input type="text" class="form-control" id="name" name="name">
                        <p class="help-block text-danger name_error"></p>
                    </div>
                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" name="email" class="form-control" id="email">
                        <p class="help-block text-danger email_error"></p>
                    </div>

                    <div class="form-group">
                        <label for="message">Message *</label>
                        <textarea id="message" name="message" cols="30" rows="5" class="form-control"></textarea>
                        <p class="help-block text-danger message_error"></p>
                    </div>
                    <div class="form-group mb-0">
                        <input type="submit" value="Leave Comment" class="btn btn-primary px-3">
                    </div>
                    <input type="hidden" name="article_slug" value="{{ $objectItem->slug }}"/>
                </form>
            </div>
        </div>

        <div class="col-lg-4 mt-5 mt-lg-0">
            <!-- Author Bio -->
            <div class="d-flex flex-column text-center bg-primary rounded mb-5 py-5 px-4">
                <img src="../frontend/img/user.jpg" class="img-fluid rounded-circle mx-auto mb-3" style="width: 100px;">
                <h3 class="text-secondary mb-3">John Doe</h3>
                <p class="text-white m-0">Conset elitr erat vero dolor ipsum et diam, eos dolor lorem ipsum, ipsum ipsum sit no ut est. Guber ea ipsum erat kasd amet est elitr ea sit.</p>
            </div>

            <!-- Search Form -->
            <div class="mb-5">
                <form action="">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-lg" placeholder="Keyword">
                        <div class="input-group-append">
                            <span class="input-group-text bg-transparent text-primary"><i
                                    class="fa fa-search"></i></span>
                        </div>
                    </div>
                </form>
            </div>
            @if ($listCategory ?? '')
            <!-- Category List -->
            <div class="mb-5">
                <h2 class="mb-4">Categories</h2>
                <ul class="list-group list-group-flush">
                    @foreach ($listCategory as $itemCate)
                    <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                        <a href="/{{ $itemCate->slug }}.html">{{ $itemCate->name }}</a>
                        <span class="badge badge-primary badge-pill">{{ $itemCate->listItem->count() }}</span>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif 
            {{-- <!-- Single Image -->
            <div class="mb-5">
                <img src="../frontend/img/blog-1.jpg" alt="" class="img-fluid rounded">
            </div>

            <!-- Recent Post -->
            <div class="mb-5">
                <h2 class="mb-4">Recent Post</h2>
                <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mb-3">
                    <img class="img-fluid" src="../frontend/img/post-1.jpg" style="width: 80px; height: 80px;">
                    <div class="pl-3">
                        <h5 class="">Diam amet eos at no eos</h5>
                        <div class="d-flex">
                            <small class="mr-3"><i class="fa fa-user text-primary"></i> Admin</small>
                            <small class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</small>
                            <small class="mr-3"><i class="fa fa-comments text-primary"></i> 15</small>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mb-3">
                    <img class="img-fluid" src="../frontend/img/post-2.jpg" style="width: 80px; height: 80px;">
                    <div class="pl-3">
                        <h5 class="">Diam amet eos at no eos</h5>
                        <div class="d-flex">
                            <small class="mr-3"><i class="fa fa-user text-primary"></i> Admin</small>
                            <small class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</small>
                            <small class="mr-3"><i class="fa fa-comments text-primary"></i> 15</small>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center bg-light shadow-sm rounded overflow-hidden mb-3">
                    <img class="img-fluid" src="../frontend/img/post-3.jpg" style="width: 80px; height: 80px;">
                    <div class="pl-3">
                        <h5 class="">Diam amet eos at no eos</h5>
                        <div class="d-flex">
                            <small class="mr-3"><i class="fa fa-user text-primary"></i> Admin</small>
                            <small class="mr-3"><i class="fa fa-folder text-primary"></i> Web Design</small>
                            <small class="mr-3"><i class="fa fa-comments text-primary"></i> 15</small>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Image -->
            <div class="mb-5">
                <img src="../frontend/img/blog-2.jpg" alt="" class="img-fluid rounded">
            </div>

            <!-- Tag Cloud -->
            <div class="mb-5">
                <h2 class="mb-4">Tag Cloud</h2>
                <div class="d-flex flex-wrap m-n1">
                    <a href="" class="btn btn-outline-primary m-1">Design</a>
                    <a href="" class="btn btn-outline-primary m-1">Development</a>
                    <a href="" class="btn btn-outline-primary m-1">Marketing</a>
                    <a href="" class="btn btn-outline-primary m-1">SEO</a>
                    <a href="" class="btn btn-outline-primary m-1">Writing</a>
                    <a href="" class="btn btn-outline-primary m-1">Consulting</a>
                </div>
            </div>

            <!-- Single Image -->
            <div class="mb-5">
                <img src="../frontend/img/blog-3.jpg" alt="" class="img-fluid rounded">
            </div>

            <!-- Plain Text -->
            <div>
                <h2 class="mb-4">Plain Text</h2>
                Aliquyam sed lorem stet diam dolor sed ut sit. Ut sanctus erat ea est aliquyam dolor et. Et no consetetur eos labore ea erat voluptua et. Et aliquyam dolore sed erat. Magna sanctus sed eos tempor rebum dolor, tempor takimata clita sit et elitr ut eirmod.
            </div> --}}
        </div>
    </div>
</div>
@endif