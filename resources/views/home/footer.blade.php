<div class="container-fluid bg-secondary text-white py-5 px-sm-3 px-md-5">
    <div class="row pt-5">
        <div class="col-lg-3 col-md-6 mb-5">
            <a href="" class="navbar-brand font-weight-bold text-primary m-0 mb-4 p-0" style="font-size: 40px; line-height: 40px;">
                <i class="flaticon-043-teddy-bear"></i>
                <span class="text-white">@if ($configData['name'] ?? '') {{ $configData['name'] }} @endif</span>
            </a> 
            <p>@if ($configData['sapo'] ?? '') {{ $configData['sapo'] }} @endif</p>
            <div class="d-flex justify-content-start mt-4">
                <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                    style="width: 38px; height: 38px;" href="@if ($configData['twitter'] ?? '') {{ $configData['twitter'] }} @endif"><i class="fab fa-twitter"></i></a>
                <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                    style="width: 38px; height: 38px;" href="@if ($configData['facebook'] ?? '') {{ $configData['facebook'] }} @endif"><i class="fab fa-facebook-f"></i></a>
                <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                    style="width: 38px; height: 38px;" href="@if ($configData['linkedin'] ?? '') {{ $configData['linkedin'] }} @endif"><i class="fab fa-linkedin-in"></i></a>
                <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0"
                    style="width: 38px; height: 38px;" href="@if ($configData['instagram'] ?? '') {{ $configData['instagram'] }} @endif"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 mb-5">
            <h3 class="text-primary mb-4">{{__('messages.get_in_touch')}}</h3>
            @if ($configData['address'] ?? '')
            <div class="d-flex">
                <h4 class="fa fa-map-marker-alt text-primary"></h4>
                <div class="pl-3">
                    <h5 class="text-white">{{__('messages.address')}}</h5>
                    <p>{{ $configData['address'] }}</p>
                </div>
            </div>
            @endif
            @if ($configData['email'] ?? '')
            <div class="d-flex">
                <h4 class="fa fa-envelope text-primary"></h4>
                <div class="pl-3">
                    <h5 class="text-white">{{__('messages.email')}}</h5>
                    <p>{{ $configData['email'] }}</p> 
                </div>
            </div>
            @endif
            @if ($configData['tel'] ?? '')
            <div class="d-flex">
                <h4 class="fa fa-phone-alt text-primary"></h4>
                <div class="pl-3">
                    <h5 class="text-white">{{__('messages.tel')}}</h5>
                    <p>{{ $configData['tel'] }}</p>
                </div>
            </div>
            @endif
        </div>
        @if ($homeMenus ?? '')
        <div class="col-lg-3 col-md-6 mb-5">
            <h3 class="text-primary mb-4">{{__('messages.quick_links')}}</h3>
            <div class="d-flex flex-column justify-content-start">
                @foreach ($homeMenus as $itemMenu)
                @if (unserialize($itemMenu->properties)['url'] ?? '')
                <a class="text-white mb-2 @if ('/'.Request::segment(1) == unserialize($itemMenu->properties)['url']) active @endif" href="{{ unserialize($itemMenu->properties)['url'] }}"><i class="fa fa-angle-right mr-2"></i>{{ $itemMenu->name }}</a>
                @endif
                @endforeach
            </div>
        </div>
        @endif 
        <div class="col-lg-3 col-md-6 mb-5">
            <h3 class="text-primary mb-4">{{__('messages.news_letter')}}</h3>
            <form action="{{ route('home.saveNewsletter') }}" method="post" id="newsletter_form">
                @csrf
                <div class="form-group">
                    <input type="text" name="name" class="form-control border-0 py-4" placeholder="{{__('messages.your_name')}}" required="required" />
                    <p class="help-block text-danger name_error"></p>
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control border-0 py-4" placeholder="{{__('messages.your_email')}}" required="required" />
                        <p class="help-block text-danger email_error"></p>
                </div>
                <div>
                    <button class="btn btn-primary btn-block border-0 py-3" type="submit">{{__('messages.submit_now')}}</button>
                </div>
            </form>
        </div>
    </div>
    @if ($configData['copyright'] ?? '')
    <div class="container-fluid pt-5" style="border-top: 1px solid rgba(23, 162, 184, .2);;">
        <p class="m-0 text-center text-white">
            {!! $configData['copyright'] !!}
        </p>
    </div>
    @endif
</div>
<!-- Back to Top -->
<a href="#" class="btn btn-primary p-3 back-to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- JavaScript Libraries -->
{{-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> --}}
<script src="{{ url('/') }}/frontend/js/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="{{ url('/') }}/frontend/lib/easing/easing.min.js"></script>
<script src="{{ url('/') }}/frontend/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="{{ url('/') }}/frontend/lib/isotope/isotope.pkgd.min.js"></script>
<script src="{{ url('/') }}/frontend/lib/lightbox/js/lightbox.min.js"></script>

<!-- Contact Javascript File -->
<script src="{{ url('/') }}/frontend/mail/jqBootstrapValidation.min.js"></script>
{{-- <script src="{{ url('/') }}/frontend/mail/contact.js"></script> --}}

<!-- Template Javascript -->
<script src="{{ url('/') }}/frontend/js/sweetalert2.js"></script>
<script src="{{ url('/') }}/frontend/js/main.js?v={{ $configData['version_home'] }}"></script>
<script src="{{ url('/') }}/frontend/js/new.js?v={{ $configData['version_home'] }}"></script>
</body>