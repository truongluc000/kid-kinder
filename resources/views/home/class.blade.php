@if ($classesItem ?? '')
<div class="container-fluid pt-5">
    <div class="container"> 
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">{{ $classesItem->name }}</span></p>
            <h1 class="mb-4">{{ $classesItem->getProperty('keywords') }}</h1>
        </div>
        @if ($classesList ?? '')
        <div class="row">
            @foreach ($classesList as $itemClass)
            <div class="col-lg-4 mb-5">
                <div class="card border-0 bg-light shadow-sm pb-2">
                    <img class="card-img-top mb-2" src="{{ $itemClass->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="">
                    <div class="card-body text-center">
                        <h4 class="card-title">{{ $itemClass->name }}</h4>
                        <p class="card-text">{{ $itemClass->getProperty('sapo') }}</p>
                    </div>
                    <div class="card-footer bg-transparent py-4 px-5">
                        <div class="row border-bottom">
                            <div class="col-6 py-1 text-right border-right"><strong>{{__('messages.age_of_kid')}}</strong></div>
                            <div class="col-6 py-1">{{ $itemClass->getProperty('age') }}</div>
                        </div>
                        <div class="row border-bottom">
                            <div class="col-6 py-1 text-right border-right"><strong>{{__('messages.total_seats')}}</strong></div>
                            <div class="col-6 py-1">{{ $itemClass->getProperty('total') }}</div>
                        </div>
                        <div class="row border-bottom">
                            <div class="col-6 py-1 text-right border-right"><strong>{{__('messages.class_time')}}</strong></div>
                            <div class="col-6 py-1">{{ $itemClass->getProperty('time') }}</div>
                        </div>
                        <div class="row">
                            <div class="col-6 py-1 text-right border-right"><strong>{{__('messages.tution_fee')}}</strong></div>
                            <div class="col-6 py-1">{{ $itemClass->getProperty('fee') }}</div>
                        </div>
                    </div>
                    <a href="" class="btn btn-primary px-4 mx-auto mb-4">{{__('messages.join_now')}}</a>
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>
 <!-- Registration Start -->
<div class="container-fluid py-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 mb-5 mb-lg-0">
                {!! $classesItem->getProperty('details') !!}
                <a href="" class="btn btn-primary mt-4 py-2 px-4">{{__('messages.book_now')}}</a>
            </div>
            <div class="col-lg-5">
                <div class="card border-0">
                    <div class="card-header bg-secondary text-center p-4">
                        <h1 class="text-white m-0">{{__('messages.book_a_seat')}}</h1>
                    </div>
                    <div class="card-body rounded-bottom bg-primary p-5">
                        <form action="{{ route('home.bookASeat') }}" method="post" role="form" id="book_a_seat">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="name" class="form-control border-0 p-4" placeholder="{{__('messages.your_name')}}" required="required" />
                                <p class="help-block text-danger name_error"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control border-0 p-4" placeholder="{{__('messages.your_email')}}" required="required" />
                                <p class="help-block text-danger email_error"></p>
                            </div>
                            <div class="form-group">
                                <select name="classes" class="custom-select border-0 px-4" style="height: 47px;">
                                    <option value="0" selected>{{__('messages.select_a_class')}}</option>
                                    @if ($classesList)
                                    @foreach ($classesList as $itemClass)
                                    <option value="{{ $itemClass->id }}">{{ $itemClass->name }}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <p class="help-block text-danger classes_error"></p>
                            </div>
                            <div>
                                <button class="btn btn-secondary btn-block border-0 py-3" type="submit">{{__('messages.book_now')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Registration End -->
@endif