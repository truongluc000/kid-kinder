@if ($articleList  ?? '')
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Latest Blog</span></p>
            <h1 class="mb-4">Latest Articles From Blog</h1>
        </div>
        <div class="row pb-3">
            @foreach ($articleList as $itemArticle)
            <div class="col-lg-4 mb-4">
                <div class="card border-0 shadow-sm mb-2">
                    <img class="card-img-top mb-2" src="{{ $itemArticle->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="">
                    <div class="card-body bg-light text-center p-4">
                        <h4 class="">{{ $itemArticle->name }}</h4>
                        <div class="d-flex justify-content-center mb-3">
                            @if($itemArticle->author ?? '')<small class="mr-3"><i class="fa fa-user text-primary"></i> {{ $itemArticle->author->name }} </small>@endif
                            @if($itemArticle->categories ?? '')<small class="mr-3"><i class="fa fa-folder text-primary"></i> {{ $itemArticle->categories->name }} </small>@endif
                            @if($itemArticle->listComments ?? '')<small class="mr-3"><i class="fa fa-comments text-primary"></i> {{ count($itemArticle->listComments) }}</small>@endif
                        </div>
                        <p>{{ $itemArticle->getProperty('sapo') }}</p>
                        <a href="/detail.html" class="btn btn-primary px-4 mx-auto my-2">{{__('messages.read_more')}}</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif