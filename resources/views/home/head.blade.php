<head>
    <meta charset="utf-8"> 
    <title>@if ($objectItem ?? '') {{ $objectItem->name}} @else{{ $configData['name'] }}@endif</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="@if (!empty($objectItem) && !empty($objectItem->getProperty('keywords'))){{ $objectItem->getProperty('keywords') }}@else{{ $configData['keywords'] }}@endif" name="keywords">
    <meta content="@if (!empty($objectItem) && !empty($objectItem->getProperty('sapo'))){{ $objectItem->getProperty('sapo') }}@else{{ $configData['sapo'] }}@endif" name="description">
    @if (!empty($objectItem) && !empty($objectItem->getProperty('avatar')))    
        <meta property="og:image" content="{{ $objectItem->getProperty('avatar') }}" />   
    @else
        @if ($configData['avatar'] ?? '')    
        <meta property="og:image" content="{{ $configData['avatar'] }}" />   
        @endif
    @endif
    <meta property="og:image:width" content="384" />
    <meta property="og:image:height" content="384" />
    <meta property="og:image:alt" content="image-share" />
    <meta property="og:title" content="@if ($objectItem ?? '') {{ $objectItem->name}} @else{{ $configData['name'] }}@endif" />
    <meta property="og:description" content="@if (!empty($objectItem) && !empty($objectItem->getProperty('sapo'))){{ $objectItem->getProperty('sapo') }}@else{{ $configData['sapo'] }}@endif" />
    <meta property="og:url" content="{{ Request::fullUrl() }}" />        
    <meta property="og:site_name" content="{{ $configData['name'] }}" />
    <meta property="og:type" content="website" />
    <!-- Favicon -->
    @if ($configData['favicon'] ?? '')
    <link href="{{ $configData['favicon'] }}?v={{ $configData['version_home'] }}" rel="icon">
    @endif
    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Handlee&family=Nunito&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- Flaticon Font -->
    <link href="{{ url('/') }}/frontend/lib/flaticon/font/flaticon.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="{{ url('/') }}/frontend/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/frontend/lib/lightbox/css/lightbox.min.css" rel="stylesheet">
    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ url('/') }}/frontend/css/style.css?v={{ $configData['version_home'] }}" rel="stylesheet">
</head>
<body>  