@if ($teacherList ?? '')
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">Our Teachers</span></p>
            <h1 class="mb-4">Meet Our Teachers</h1>
        </div>
        <div class="row">
            @foreach ($teacherList as $itemTeacher)            
            <div class="col-md-6 col-lg-3 text-center team mb-5">
                <div class="position-relative overflow-hidden mb-4" style="border-radius: 100%;">
                    <img class="img-fluid w-100" src="{{ $itemTeacher->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="{{ $itemTeacher->name }}" >
                    <div
                        class="team-social d-flex align-items-center justify-content-center w-100 h-100 position-absolute">
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                            href="{{ $itemTeacher->getProperty('twitter') }}"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-outline-light text-center mr-2 px-0" style="width: 38px; height: 38px;"
                            href="{{ $itemTeacher->getProperty('facebook') }}"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-outline-light text-center px-0" style="width: 38px; height: 38px;"
                            href="{{ $itemTeacher->getProperty('linkedin') }}"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <h4>{{ $itemTeacher->name }}</h4> 
                <i>{{ $itemTeacher->getProperty('sapo') }}</i>
            </div>
            @endforeach
        </div>
    </div> 
</div>
@endif