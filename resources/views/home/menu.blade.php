<div class="container-fluid bg-light position-relative shadow">
    <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0 px-lg-5">
        <a href="/" class="navbar-brand font-weight-bold text-secondary" style="font-size: 50px;">
            @if ($configData['avatar'] ?? '')<img src="{{ $configData['avatar'] }}?v={{ $configData['version_home'] }}">@endif
            <span class="text-primary">@if ($configData['name'] ?? '') {{ $configData['name'] }} @endif</span>
        </a>
        @if ($homeMenus ?? '')
        {{-- menu from app service provider --}}
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
            <div class="navbar-nav font-weight-bold mx-auto py-0">
                @foreach ($homeMenus as $itemMenu)
                    @if (unserialize($itemMenu->properties)['url'] ?? '')
                    <a href="{{ unserialize($itemMenu->properties)['url'] }}" class="nav-item nav-link @if ('/'.Request::segment(1) == unserialize($itemMenu->properties)['url']) active @endif">{{ $itemMenu->name }}</a>
                    {{-- <a href="{{ unserialize($itemMenu->properties)['url'] }}" class="nav-item nav-link {{ request()->is(''.unserialize($itemMenu->properties)['url'].'') ? 'active' : '' }}">{{ $itemMenu->name }}</a> --}}
                    {{-- couldn't use request()->is() because the url from database got '/' --}}
                    @endif
                @endforeach
            </div>
            <a href="/our-class.html" class="btn btn-primary px-4">{{__('messages.join_class')}}</a>
        </div>
        @endif
    </nav> 
</div>