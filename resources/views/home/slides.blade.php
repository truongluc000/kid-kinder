<!-- Header Start -->
@if ($adsList ?? '')
    @foreach ($adsList as $itemData)
        @if ($itemData->categories->id == 1 && $loop->iteration == 1)
        {{-- 1st item of sliders category --}}
            <div class="container-fluid bg-primary px-0 px-md-5 mb-5">
                <div class="row align-items-center px-3">
                    <div class="col-lg-6 text-center text-lg-left">
                        <h4 class="text-white mb-4 mt-5 mt-lg-0">{{ $itemData->name }}</h4>
                        <h1 class="display-3 font-weight-bold text-white">@if ($itemData->getProperty('keywords') ?? '') {{ $itemData->getProperty('keywords') }} @endif</h1>
                        <p class="text-white mb-4">@if ($itemData->getProperty('sapo') ?? '') {{ $itemData->getProperty('sapo') }} @endif</p>
                        <a href="/our-class.html" class="btn btn-secondary mt-1 py-3 px-5">{{__('messages.learn_more')}}</a>
                    </div> 
                    @if ($itemData->getProperty('avatar'))
                    <div class="col-lg-6 text-center text-lg-right">
                        <img class="img-fluid mt-5" src="{{ $itemData->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="{{ $itemData->name }}">
                    </div>
                    @endif
                </div>
            </div>
        @endif
    @endforeach
@endif 
<!-- Header End -->