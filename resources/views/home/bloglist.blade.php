@if (!empty($blogItem))
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">{{ $blogItem->name }}</span></p>
            <h1 class="mb-4">{{ $blogItem->getProperty('keywords') }}</h1>
        </div>
        <div class="row pb-3">
            @if ($list ?? '') 
            @foreach ($list as $itemBlogEa) 
            <div class="col-lg-4 mb-4">
                <div class="card border-0 shadow-sm mb-2">
                    <img class="card-img-top mb-2" src="{{ $itemBlogEa->getProperty('avatar') }}?v={{ $configData['version_home'] }}" alt="">
                    <div class="card-body bg-light text-center p-4">
                        <h4 class="">{{ $itemBlogEa->name }}</h4>
                        <div class="d-flex justify-content-center mb-3">
                            @if($itemBlogEa->author ?? '')<small class="mr-3"><i class="fa fa-user text-primary"></i> {{$itemBlogEa->author->name}} </small>@endif
                            @if($itemBlogEa->categories ?? '')<small class="mr-3"><i class="fa fa-folder text-primary"></i> {{$itemBlogEa->categories->name}} </small>@endif
                            @if($itemBlogEa->listComments ?? '')<small class="mr-3"><i class="fa fa-comments text-primary"></i> {{ count($itemBlogEa->listComments) }}</small>@endif
                        </div>
                        <p>{{ $itemBlogEa->getProperty('sapo') }}</p>
                        @if($itemBlogEa->categories ?? '')
                        <a href="/{{$itemBlogEa->categories->slug}}/{{$itemBlogEa->slug}}.html" class="btn btn-primary px-4 mx-auto my-2">{{__('messages.read_more')}}</a>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
                {{ $list->onEachSide(config('constants.numPageDisplay'))->links('vendor.pagination.bootstrap') }}  
            @endif
        </div>
    </div>
</div>
@endif