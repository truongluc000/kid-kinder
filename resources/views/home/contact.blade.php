@if ($contactItem ?? '')
<div class="container-fluid pt-5">
        <div class="container">
            <div class="text-center pb-2">
                <p class="section-title px-5"><span class="px-2">{{ $contactItem->name }}</span></p>
                <h1 class="mb-4">{{ $contactItem->getProperty('keywords') }}</h1>
            </div>
            <div class="row">
                <div class="col-lg-7 mb-5">
                    <div class="contact-form"> 
                        <div id="success"></div>  
                        <form action="{{ route('home.saveContact') }}" method="post" name="sentMessage" id="contact_form" novalidate="novalidate">
                            @csrf
                            <div class="control-group">
                                <input type="text" class="form-control" name="name" id="name" placeholder="{{__('messages.your_name')}}" required="required" data-validation-required-message=" {{__('messages.name_is_required')}}" />
                                <p class="help-block text-danger name_error"></p>
                            </div> 
                            <div class="control-group">
                                <input type="email" class="form-control" name="email" id="email"  placeholder="{{__('messages.your_email')}}" required="required" data-validation-required-message="{{__('messages.email_is_required')}}" />
                                <p class="help-block text-danger email_error"></p>
                            </div> 
                            <div class="control-group">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="{{__('messages.subject')}}" required="required" data-validation-required-message="{{__('messages.subject_is_required')}}" />
                                <p class="help-block text-danger subject_error"></p>
                            </div>
                            <div class="control-group"> 
                                <textarea class="form-control" rows="6" name="message" id="message" placeholder="{{__('messages.message')}}" required="required" data-validation-required-message="{{__('messages.message_is_required')}}"></textarea>
                                <p class="help-block text-danger message_error"></p>
                            </div>
                            <div>
                                <button class="btn btn-primary py-2 px-4" type="submit" id="sendMessageButton">{{__('messages.send_message')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-lg-5 mb-5">
                    <p>{{ $contactItem->getProperty('sapo') }}</p>
                    <div class="d-flex">
                        <i class="fa fa-map-marker-alt d-inline-flex align-items-center justify-content-center bg-primary text-secondary rounded-circle" style="width: 45px; height: 45px;"></i>
                        <div class="pl-3">
                            <h5>{{__('messages.address')}}</h5>
                            <p>{{ $configData['address'] }}</p>
                        </div>
                    </div>
                    <div class="d-flex">
                        <i class="fa fa-envelope d-inline-flex align-items-center justify-content-center bg-primary text-secondary rounded-circle" style="width: 45px; height: 45px;"></i>
                        <div class="pl-3">
                            <h5>{{__('messages.email')}}</h5>
                            <p>{{ $configData['email'] }}</p>
                        </div>
                    </div>
                    <div class="d-flex">
                        <i class="fa fa-phone-alt d-inline-flex align-items-center justify-content-center bg-primary text-secondary rounded-circle" style="width: 45px; height: 45px;"></i>
                        <div class="pl-3">
                            <h5>{{__('messages.tel')}}</h5>
                            <p>{{ $configData['tel'] }}</p>
                        </div>
                    </div>
                    <div class="d-flex">
                        <i class="far fa-clock d-inline-flex align-items-center justify-content-center bg-primary text-secondary rounded-circle" style="width: 45px; height: 45px;"></i>
                        <div class="pl-3">
                            <h5>{{__('messages.opening_hours')}}</h5>
                            <strong>{{ $configData['opening_days'] }}:</strong>
                            <p class="m-0">{{ $configData['opening_hours'] }} </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
