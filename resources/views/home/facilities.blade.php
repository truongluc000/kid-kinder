<div class="container-fluid pt-5">
    <div class="container pb-3">
        <div class="row">
            @if ($adsList ?? '') 
                @foreach ($adsList as $itemFacilities)
                    @if ($itemFacilities->categories->id == 2)
                    {{-- 2nd item is Facilities category --}}
                    <div class="col-lg-4 col-md-6 pb-1">
                        <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 30px;">
                            <i class="@if ($itemFacilities->getProperty('css_class')) {{ $itemFacilities->getProperty('css_class') }} @endif
                                h1 font-weight-normal text-primary mb-3"></i>
                            <div class="pl-4">
                                <h4>{{ $itemFacilities->name }}</h4>
                                <p class="m-0">@if ($itemFacilities->getProperty('sapo')) {{ $itemFacilities->getProperty('sapo') }} @endif</p>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            @endif
        </div>
    </div>
</div>