@include(''.config('constants.adminLink').'.include.head') 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.list')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title ">
                  @include(''.config('constants.adminLink').'.include.alert')        
                  <form action="" method="get" name="formTypesearch" id="formTypesearch" class="floatright">
                    {{ csrf_field() }}
                    {{-- <select class="form-control width150px inlineblock" name="pId">
                    <option value="0">All</option>
                    </select> --}}
                    <input type="text" class="form-control inlineblock width120px" placeholder="{{__('messages.search')}}" value="" autocomplete="off" name="kw">
                    <input type="button" value="{{__('messages.search')}}" title="{{__('messages.search')}}" name="btnSearch" class="btn inlineblock btn-success" onclick="submitFormSearch()">
                    @if (Auth::user()->can('add_article_category') || Auth::user()->hasrole('admin'))
                    <a class="btn inlineblock btn-primary" title="{{__('messages.add_new')}}" href="{{ route('articlecategories.create') }}">{{__('messages.add_new')}}</a>
                    @endif
                    <input type="hidden" name="object" value="{{ Request::segment(2) }}">
                  </form>
                  <ul class="nav navbar-right panel_toolbox">
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="row">
                    <div class="col-sm-12" style="width: 100%">
                      <div class="card-box table-responsive overflowhy">
          <form action="{{ route('articlecategories.update') }}" method="post" name="formType" id="formType" role="form">
            {{ csrf_field() }} 
            <table class="table table-striped table-bordered bulk_action">
              <thead>
          <tr>
            <th><input type="checkbox" name="all" id="check-all" value="1" class="flat "></th>
            <th>{{__('messages.no')}}</th>
            <th>{{__('messages.id')}}</th>
            <th>{{__('messages.avatar')}}</th>
            <th>{{__('messages.title')}}</th>
            <th>{{__('messages.slug')}}</th> 
            <th>{{__('messages.created')}}/ {{__('messages.updated')}}</th>
            <th>{{__('messages.status')}}</th>
            <th style="width: 85px;">{{__('messages.action')}}</th>
          </tr>
          </thead>
          <tbody>
            @if ($list)
            @foreach ($list as $k => $item)
              <tr class="@if ($k%2 == 0) even @else odd @endif pointer">
                <td><input type="checkbox" name="ids[]" value="{{$item->id}}" class="flat checkbox-input"></td>
                <td>{{$k+1}}</td>
                <td>{{$item->id}}</td> 
                <td><img src="{{ $item->getProperty('avatar') }}" width="100px"></td>
                <td>{{$item->name}}</td>
                <td>{{$item->slug}}</td>
                <td>{{$item->created_at}}<br>{{$item->updated_at}}</td>
                <td>{!! config('constants.statusTextBackend.'.$item->status) !!}</td> 
                <td class="actionicon">
                @if (Auth::user()->can('edit_article_category') || Auth::user()->hasrole('admin'))
                <a href="{{ route('articlecategories.show', ['id' => $item->id]) }}" title="{{__('messages.update_msg')}}"><i class="fa fa-pencil yellowcolor"></i></a>
                <a href="{{ route('articlecategories.changeStatus', ['status' => config('constants.enable'),'id' => $item->id]) }}" title="{{__('messages.enable_msg')}}"><i class="fa fa-check greencolor"></i></a>
                <a href="{{ route('articlecategories.changeStatus', ['status' => config('constants.disable'),'id' => $item->id]) }}" title="{{__('messages.disable_msg')}}"><i class="fa fa-close redcolor"></i></a>
                <a onclick="return confirm('{{__("messages.confirm_delete")}}')" href="{{ route('articlecategories.changeStatus', ['status' => config('constants.delete'),'id' => $item->id]) }}" title="{{__('messages.delete_msg')}}"><i class="fa fa-trash redcolor"></i></a>
                @endif
              </td>
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          <div class="">
            <div class="col-md-12">   
            @include(''.config('constants.adminLink').'.include.pagination')
            <div class="col-sm-12 col-xs-12 col-md-5">
              <div class="table-responsive">
                <p class="listBtn floatright">
                  @if (Auth::user()->can('edit_article_category') || Auth::user()->hasrole('admin'))
                  <input type="hidden" name="new_status" id="new_status"/>
                  {{-- <button type="button" class="btn btn-info" onclick="changeStatus();">{{__('messages.update_msg')}}</button> --}}
                  <button type="button" class="btn btn-success" onclick="changeStatus(1);">{{__('messages.enable_msg')}}</button>
                  <button type="button" class="btn btn-danger" onclick="changeStatus(0);">{{__('messages.disable_msg')}}</button>
                  <button type="button" class="btn btn-danger" onclick="changeStatus(2);">{{__('messages.delete_msg')}}</button>
                  @endif
                </p>
              </div>
            </div>
          </div>
        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
