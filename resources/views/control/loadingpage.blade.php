<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{ url('/') }}/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('/') }}/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('/') }}/backend/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ url('/') }}/backend/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ url('/') }}/backend/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <h1>Redirect</h1>
            @if (session('success'))
            <div class="alert alert-success">
              <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
              <strong>Tốt!</strong> {{ session('success') }} 
            </div>
            @endif
            <div class="clearfix"></div>
            <div class="separator">
              <div class="clearfix"></div>
              <br />
            </div>
          </section>
        </div>

      </div>
    </div>
  </body>
  <script type="text/javascript">
		
		function Redirect() {
			window.location='/admin';
		}
		setTimeout('Redirect()',1000);
      </script>
</html>
