@include(''.config('constants.adminLink').'.include.head') 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.list')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title "> 
                  @include(''.config('constants.adminLink').'.include.alert')        
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="row">
                    <div class="col-sm-12" style="width: 100%">
                      <div class="card-box table-responsive overflowhy">
                        <div id="ckfinder-widget"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
        <script>
          CKFinder.widget( 'ckfinder-widget', {
          language: 'en',
          width: '100%',
          height: 750,
          id:'custom-listview',
          } );
        </script>
