@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content --> 
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.update_msg')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  @include(''.config('constants.adminLink').'.include.alert')     
                  @if ($item)  
                  <form action="{{ route('config.edit', ['id' => $item->id]) }}" method="post" role="form">
                    {{ csrf_field() }}
                    @method('PUT')
                    <fieldset>
                      @include(''.config('constants.adminLink').'.fields.update.name')
                      @include(''.config('constants.adminLink').'.fields.update.keywords')
                      @include(''.config('constants.adminLink').'.fields.update.sapo')
                      @include(''.config('constants.adminLink').'.fields.update.copyright')
                      @include(''.config('constants.adminLink').'.fields.update.email_config')
                      @include(''.config('constants.adminLink').'.fields.update.tel_config')
                      @include(''.config('constants.adminLink').'.fields.update.address_config')
                      @include(''.config('constants.adminLink').'.fields.update.twitter')
                      @include(''.config('constants.adminLink').'.fields.update.facebook')
                      @include(''.config('constants.adminLink').'.fields.update.linkedin')
                      @include(''.config('constants.adminLink').'.fields.update.instagram')
                      @include(''.config('constants.adminLink').'.fields.update.avatar')
                      @include(''.config('constants.adminLink').'.fields.update.favicon')
                      @include(''.config('constants.adminLink').'.fields.update.version')
                      @include(''.config('constants.adminLink').'.fields.update.opening_config')
                      @include(''.config('constants.adminLink').'.fields.update.submit')
                    </fieldset>
                  </form>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
