@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body"> 
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content --> 
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.update_msg')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  @include(''.config('constants.adminLink').'.include.alert')     
                  @if ($item)  
                    <form action="{{ route('user.edit', ['id' => $item->id]) }}" method="post" role="form">
                      {{ csrf_field() }}
                      @method('PUT') 
                      <fieldset>
                        @include(''.config('constants.adminLink').'.fields.update.level')
                        @include(''.config('constants.adminLink').'.fields.update.status')
                        @include(''.config('constants.adminLink').'.fields.update.fullname')
                        @include(''.config('constants.adminLink').'.fields.update.email')
                        @include(''.config('constants.adminLink').'.fields.update.password')
                        @include(''.config('constants.adminLink').'.fields.update.password_confirmation')
                        @include(''.config('constants.adminLink').'.fields.update.avatar')

                        @include(''.config('constants.adminLink').'.fields.update.submit')
                      </fieldset>
                    </form>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
