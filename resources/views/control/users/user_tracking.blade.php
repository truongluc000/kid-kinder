@include(''.config('constants.adminLink').'.include.head') 
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.list')}}<small></small></h3>
            </div>
          </div> 
          <div class="clearfix"></div> 
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title ">
                  @include(''.config('constants.adminLink').'.include.alert')        
                  <form action="" method="get" name="formTypesearch" id="formTypesearch" class="floatright">
                    {{ csrf_field() }}
                    {{-- <select class="form-control width150px inlineblock" name="pId">
                      <option value="0" selected>{{__('messages.all')}}</option>
                    </select> --}}
                    <input type="text" class="form-control inlineblock width120px" placeholder="{{__('messages.search')}}" value="" autocomplete="off" name="kw">
                    <input type="button" value="{{__('messages.search')}}" title="{{__('messages.search')}}" name="btnSearch" class="btn inlineblock btn-success" onclick="submitFormSearch()">
                    <input type="hidden" name="object" value="trackings">
                  </form>
                  <ul class="nav navbar-right panel_toolbox">
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="row">
                    <div class="col-sm-12" style="width: 100%">
                      <div class="card-box table-responsive overflowhy">
          <form action="{{ route('user.trackings') }}" method="post" name="formType" id="formType" role="form">
            {{ csrf_field() }} 
            <table class="table table-striped table-bordered bulk_action">
              <thead>
          <tr>
            <th><input type="checkbox" name="all" id="check-all" value="1" class="flat "></th>
            <th>{{__('messages.no')}}</th>
            <th>{{__('messages.id')}}</th>
            <th>{{__('messages.username')}}</th>
            <th class="w_40_vw">{{__('messages.action')}}</th>
            <th>{{__('messages.url')}}</th>
            <th style="width:150px;">{{__('messages.created')}}</th>
            {{-- <th>{{__('messages.status')}}</th> --}}
          </tr>
          </thead>
          <tbody>
            @if ($list)
            @foreach ($list as $k => $item)
              <tr class="@if ($k%2 == 0) even @else odd @endif pointer">
                <td><input type="checkbox" name="ids[]" value="{{$item->id}}" class="flat checkbox-input"></td>
                <td>{{$k+1}}</td>
                <td>{{$item->id}}</td> 
                <td>{{$item->username}}</td>
                <td class="action_log" title="{{$item->action}}">{{$item->action}}</td>
                <td title="{{$item->getProperty('cur_url')}}">{{$item->getProperty('cur_url')}}</td>
                <td>{{$item->created_at}}</td>
                {{-- <td>{!! config('constants.statusTextBackend.'.$item->status) !!}</td>  --}}
              </tr>
              @endforeach
              @endif
            </tbody>
          </table>
          <div class="">
            <div class="col-md-12">   
            @include(''.config('constants.adminLink').'.include.pagination')
            <div class="col-sm-12 col-xs-12 col-md-5">
              <div class="table-responsive">
                {{-- <p class="listBtn floatright">
                  <input type="hidden" name="new_status" id="new_status"/>
                  <button type="button" class="btn btn-danger" onclick="changeStatus(2);">{{__('messages.delete_msg')}}</button>
                </p> --}}
              </div>
            </div>
          </div>
        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
