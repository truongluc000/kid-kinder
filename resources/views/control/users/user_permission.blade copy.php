@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content --> 
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.set_permission')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content bulk_action">
                  @include(''.config('constants.adminLink').'.include.alert')     
                  @if ($item)  
                    <form action="{{ url(''.config('constants.adminLink').'/'. Request::segment(2) .'/permissionUpdate/'.$item->id) }}" method="post" role="form" id="permissionForm" enctype="multipart/form-data">
                      {{ csrf_field() }}
                      @method('PUT')
                      <fieldset>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.check_all_permissions')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                          <input type="checkbox" value="1" id="check-all" class="flat">
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.menus')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput"> 
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="menu[view]" value="1" class="flat checkbox-input" @if ($item->can('view_menu'))checked="checked" @endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="menu[add]" value="1" class="flat checkbox-input" @if ($item->can('add_menu'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="menu[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_menu'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="menu[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_menu'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.classes')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="classes[view]" value="1" class="flat checkbox-input" @if ($item->can('view_classes'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="classes[add]" value="1" class="flat checkbox-input" @if ($item->can('add_classes'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="classes[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_classes'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="classes[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_classes'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.teachers')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="teacher[view]" value="1" class="flat checkbox-input" @if ($item->can('view_teacher'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="teacher[add]" value="1" class="flat checkbox-input" @if ($item->can('add_teacher'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="teacher[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_teacher'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="teacher[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_teacher'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.gallery')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="gallery[view]" value="1" class="flat checkbox-input" @if ($item->can('view_gallery'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="gallery[add]" value="1" class="flat checkbox-input" @if ($item->can('add_gallery'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="gallery[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_gallery'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="gallery[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_gallery'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.ads_categories')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="ads_category[view]" value="1" class="flat checkbox-input" @if ($item->can('view_ads_category'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="ads_category[add]" value="1" class="flat checkbox-input" @if ($item->can('add_ads_category'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="ads_category[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_ads_category'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="ads_category[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_ads_category'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.ads')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="ads[view]" value="1" class="flat checkbox-input" @if ($item->can('view_ads'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="ads[add]" value="1" class="flat checkbox-input" @if ($item->can('add_ads'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="ads[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_ads'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="ads[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_ads'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.article_categories')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="article_category[view]" value="1" class="flat checkbox-input" @if ($item->can('view_article_category'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="article_category[add]" value="1" class="flat checkbox-input" @if ($item->can('add_article_category'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="article_category[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_article_category'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="article_category[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_article_category'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.article')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="article[view]" value="1" class="flat checkbox-input" @if ($item->can('view_article'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="article[add]" value="1" class="flat checkbox-input" @if ($item->can('add_article'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="article[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_article'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="article[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_article'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.comments')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="comment[view]" value="1" class="flat checkbox-input" @if ($item->can('view_comment'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="comment[add]" value="1" class="flat checkbox-input" @if ($item->can('add_comment'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="comment[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_comment'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="comment[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_comment'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.static')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="static[view]" value="1" class="flat checkbox-input" @if ($item->can('view_static'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="static[add]" value="1" class="flat checkbox-input" @if ($item->can('add_static'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="static[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_static'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="static[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_static'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.library')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox"  value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="library[view]" value="1" class="flat checkbox-input" @if ($item->can('view_library'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="library[add]" value="1" class="flat checkbox-input" @if ($item->can('add_library'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="library[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_library'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="library[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_library'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                          </div> 
                        </div>
                        <div class="item form-group">
                          <label class="col-form-label col-md-3 col-sm-3 label-align"> {{__('messages.list_user')}}
                          </label>
                          <div class="col-md-9 col-sm-6 col-xs-12 taginput">
                            <input type="checkbox" value="1" class="flat checkbox-input all"><label class="lbl">{{__('messages.all')}}</label>
                            <input type="checkbox" name="user[view]" value="1" class="flat checkbox-input" @if ($item->can('view_user'))checked="checked"@endif><label class="lbl">{{__('messages.view')}}</label>
                            <input type="checkbox" name="user[add]" value="1" class="flat checkbox-input" @if ($item->can('add_user'))checked="checked"@endif><label class="lbl">{{__('messages.add_new')}}</label>
                            <input type="checkbox" name="user[edit]" value="1" class="flat checkbox-input" @if ($item->can('edit_user'))checked="checked"@endif><label class="lbl">{{__('messages.update_msg')}}</label>
                            <input type="checkbox" name="user[delete]" value="1"  class="flat checkbox-input" @if ($item->can('delete_user'))checked="checked"@endif><label class="lbl">{{__('messages.clean_trash')}}</label>
                            <input type="checkbox" name="user[permission]" value="1"  class="flat checkbox-input" @if ($item->can('permission_user'))checked="checked"@endif><label class="lbl">{{__('messages.set_permission')}}</label>
                          </div>
                        </div>
                        

                        @include(''.config('constants.adminLink').'.fields.update.submit')
                      </fieldset>
                    </form>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
