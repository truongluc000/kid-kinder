@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.add_new')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  @include(''.config('constants.adminLink').'.include.alert')        
                  <form action="{{ route('user.store') }}" method="post" role="form">
                    {{ csrf_field() }}
                    <fieldset>
                      @include(''.config('constants.adminLink').'.fields.create.level')
                      @include(''.config('constants.adminLink').'.fields.create.status')
                      @include(''.config('constants.adminLink').'.fields.create.fullname')
                      @include(''.config('constants.adminLink').'.fields.create.email')
                      @include(''.config('constants.adminLink').'.fields.create.password')
                      @include(''.config('constants.adminLink').'.fields.create.password_confirmation')
                      @include(''.config('constants.adminLink').'.fields.create.avatar')

                      @include(''.config('constants.adminLink').'.fields.create.submit')
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
