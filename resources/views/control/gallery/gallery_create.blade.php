@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.add_new')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  @include(''.config('constants.adminLink').'.include.alert')        
                  <form action="{{ route('gallery.store') }}" method="post" role="form" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <fieldset>
                      @include(''.config('constants.adminLink').'.fields.create.status')
                      @include(''.config('constants.adminLink').'.fields.create.position')
                      @include(''.config('constants.adminLink').'.fields.create.name')
                      @include(''.config('constants.adminLink').'.fields.create.photos')
                      @include(''.config('constants.adminLink').'.fields.create.submit')
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
