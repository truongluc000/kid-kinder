<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{__('messages.site_name')}} </title>

    <!-- Bootstrap -->
    <link href="{{ url('/') }}/backend/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ url('/') }}/backend/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ url('/') }}/backend/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ url('/') }}/backend/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ url('/') }}/backend/build/css/custom.min.css" rel="stylesheet">
    <link href="{{ url('/') }}/backend/build/css/style.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="{{ route('auth.postLogin') }}" method="POST" role="form">
              {{ csrf_field() }}
              <h1>{{__('messages.login_form')}}</h1>
              @if (session('error'))
              <div class="errorBox" style="margin-top: 20px;">
                <ul class="alert color-red" role="alert" style="list-style: none;">
                  <li>{{ session('error') }}</li>
                </ul> 
                </div>
              @endif
              <div>
                <input type="text" name="email" class="form-control" placeholder="{{__('messages.username')}}" required="" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="{{__('messages.password')}}" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">{{__('messages.login')}}</button>
                {{-- <a class="reset_pass" href="#">{{__('messages.forgot_password')}}</a> --}}
              </div>

              <div class="clearfix"></div>

              <div class="separator"> 
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> {{__('messages.register')}} </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="{{ url(''.config('constants.').'/frontend/img/kid-logo.jpg')}}"> <h1>{{__('messages.site_name')}}</h1>
                  <p>Developer by <a href="https://truongluc.com">TruongLuc.com</a></p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form"> 
          <section class="login_content">
            <form action="{{ route('auth.postRegister') }}" method="POST" role="form" id="registerForm">
              <h1>{{__('messages.create_account')}}</h1>
              {{ csrf_field() }}
              <div>
                <input type="text" name="name" class="form-control" placeholder="{{__('messages.fullname')}}" required="" />
                <p class="help-block is-danger name_error"></p>
                {{-- @if ($errors->has('name'))
                    <p class="help is-danger">{{ $errors->first('name') }}</p>
                @endif --}}
              </div> 
              <div>
                <input type="email" name="email" class="form-control" placeholder="{{__('messages.email')}}" required="" />
                <p class="help-block is-danger email_error"></p>
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="{{__('messages.password')}}" required="" />
                <p class="help-block is-danger password_error"></p>
              </div>
              <div>
                <input type="password" name="password_confirmation" class="form-control" placeholder="{{__('messages.confirm_password')}}" required="" />
                <p class="help-block is-danger password_confirmation_error"></p>
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">{{__('messages.submit')}}</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Already a member ?
                  <a href="#signin" class="to_register"> {{__('messages.login')}} </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <img src="{{ url(''.config('constants.').'/frontend/img/kid-logo.jpg')}}"> <h1>{{__('messages.site_name')}}</h1>
                  <p>Developer by <a href="https://truongluc.com">TruongLuc.com</a></p>
                </div>
              </div>
            </form>
          </section>
        </div>

        
      </div>
    </div>
    <script src="{{ url('/') }}/frontend/js/jquery-3.6.0.min.js"></script>
    <script src="{{ url('/') }}/frontend/js/sweetalert2.js"></script>
    <script src="{{ url('/') }}/backend/build/js/new.js"></script>
  </body>
</html>
