<div class="item form-group @if ($errors->get('position')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="position">{{__('messages.position')}}
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
    <input id="position" class="form-control" name="position" value="{{ old('position') }}" type="number">
    @if ($errors->get('position'))
      <p class="help is-danger">{{ $errors->first('position') }}</p>
    @endif
  </div>
</div>