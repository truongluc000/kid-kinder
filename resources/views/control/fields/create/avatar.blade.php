<div class="item form-group @if ($errors->get('avatar')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="avatar">{{__('messages.avatar')}}
  </label>
  <div class="col-md-5 col-sm-6 col-xs-12">
    <input type="text" class="form-control select_photo" name="avatar" id="ckfinder-input-1" accept="image/*">
    @if ($errors->get('avatar'))
      <p class="help is-danger">{{ $errors->first('avatar') }}</p>
    @endif
  </div>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <button type="button" class="btn btn-info" onclick="openPhotoUpload(this);">{{__('messages.select_image')}}</button>
  </div>
</div>