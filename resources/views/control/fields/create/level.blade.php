<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="level">{{__('messages.group')}}<span class="required">*</span>
  </label>
  <div class="col-md-4 col-sm-6 col-xs-12 @if ($errors->get('level')) errormsg @endif ">
    <select name="level" id="level" class="form-control">
      <option value="1">{{ config('constants.level.1') }}</option>
      @hasrole ('admin')
      <option value="2">{{ config('constants.level.2') }}</option>
      @endhasrole
    </select>
    @if ($errors->get('level'))
      <p class="help is-danger">{{ $errors->first('level') }}</p>
    @endif
  </div>
</div>