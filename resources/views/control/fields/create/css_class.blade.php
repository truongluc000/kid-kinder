<div class="item form-group @if ($errors->get('class')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="class">{{__('messages.class')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="class" class="form-control" name="class" value="{{ old('class') }}" type="text">
    @if ($errors->get('class'))
      <p class="help is-danger">{{ $errors->first('class') }}</p>
    @endif
  </div>
</div>