<div class="item form-group @if ($errors->get('fee')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="fee">{{__('messages.tution_fee')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="fee" class="form-control" name="fee" value="{{ old('fee') }}" type="text">
    @if ($errors->get('fee'))
      <p class="help is-danger">{{ $errors->first('fee') }}</p>
    @endif
  </div>
</div>