<div class="item form-group @if ($errors->get('time')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="time">{{__('messages.class_time')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="time" class="form-control" name="time" value="{{ old('time') }}" type="text">
    @if ($errors->get('time'))
      <p class="help is-danger">{{ $errors->first('time') }}</p>
    @endif
  </div>
</div>