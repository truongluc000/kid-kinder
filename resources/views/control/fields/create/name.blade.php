<div class="item form-group @if ($errors->get('name')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="name">{{__('messages.title')}} <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="name" class="form-control" name="name" value="{{ old('name') }}" type="text" required="required">
    @if ($errors->get('name'))
      <p class="help is-danger">{{ $errors->first('name') }}</p>
    @endif
  </div>
</div>