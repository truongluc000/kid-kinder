<div class="item form-group @if ($errors->get('copyright')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="copyright">{{__('messages.copyright')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="copyright" class="form-control" name="copyright" value="{{ old('copyright') }}" type="text">
    @if ($errors->get('copyright'))
      <p class="help is-danger">{{ $errors->first('copyright') }}</p>
    @endif
  </div>
</div>