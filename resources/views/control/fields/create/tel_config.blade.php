<div class="item form-group @if ($errors->get('tel')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="tel">{{__('messages.tel')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="tel" class="form-control" name="tel" value="{{ old('tel') }}" type="text">
    @if ($errors->get('tel'))
      <p class="help is-danger">{{ $errors->first('tel') }}</p>
    @endif
  </div>
</div>