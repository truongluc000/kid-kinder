<div class="item form-group @if ($errors->get('message')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="message">{{__('messages.message')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="message" class="form-control" name="message" value="{{ old('message') }}" type="text">
    @if ($errors->get('message'))
      <p class="help is-danger">{{ $errors->first('message') }}</p>
    @endif
  </div>
</div>