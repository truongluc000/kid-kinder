<div class="item form-group @if ($errors->get('url')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="url">{{__('messages.url')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="url" class="form-control" name="url" value="{{ old('url') }}" type="text">
    @if ($errors->get('url'))
      <p class="help is-danger">{{ $errors->first('url') }}</p>
    @endif
  </div>
</div>