<div class="content_photo">
  <div class="list_photo">
    <div class="item_photo">
      <div class="item form-group @if ($errors->get('photo')) errormsg @endif ">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="photo">{{__('messages.photo')}}
        </label>
        <div class="col-md-5 col-sm-6 col-xs-12">
          <input type="text" class="form-control select_photo" name="photo[]" accept="image/*">
          @if ($errors->get('photo'))
            <p class="help is-danger">{{ $errors->first('photo') }}</p>
          @endif
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <button type="button" class="btn btn-info" onclick="openPhotoUpload(this);">{{__('messages.select_image')}}</button>
        </div>
      </div>
      <div class="item form-group @if ($errors->get('position_img')) errormsg @endif ">
        <label class="col-form-label col-md-3 col-sm-3 label-align" for="position_img">{{__('messages.photo_position')}}
        </label>
        <div class="col-md-2 col-sm-6 col-xs-12">
          <input class="form-control" name="position_img[]" value="1" type="number">
          @if ($errors->get('position_img'))
            <p class="help is-danger">{{ $errors->first('position_img') }}</p>
          @endif
        </div>
      </div> 
    </div>
  </div>
  <div class="item form-group">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="add_more">
    </label>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <button type="button" class="btn btn-warning" onclick="addMorePhoto(this);">{{__('messages.add_more')}}</button>
    </div>
  </div>
</div>