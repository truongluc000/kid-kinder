<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="status">{{__('messages.status')}}<span class="required">*</span>
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
   <select name="status" id="status" class="form-control">
    <option value="1">{{ config('constants.status.1') }}</option>
    <option value="0">{{ config('constants.status.0') }}</option>
    </select>
  </div>
</div>