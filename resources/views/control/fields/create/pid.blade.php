<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="pid">{{__('messages.select_category')}}<span class="required">*</span>
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
   <select name="pid" id="pid" class="form-control">
     <option value="0">{{__('messages.default')}}</option>
     @if ($list ?? '')
       @foreach ($list as $itemList)
        <option value="{{$itemList->id}}">{{$itemList->name}}</option>
      @endforeach
     @endif 
    </select>
  </div>
</div>