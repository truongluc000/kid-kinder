<div class="item form-group @if ($errors->get('details')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="details">{{__('messages.details')}}
  </label>
</div>
<div class="item form-group @if ($errors->get('details')) errormsg @endif ">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <textarea class="form-control" name="details" id="details" cols="10" rows="3">{{ old('details') }}</textarea>
        @if ($errors->get('details'))
          <p class="help is-danger">{{ $errors->first('details') }}</p>
        @endif
  </div>
</div>
<script>CKEDITOR.replace( 'details', {filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',} );</script>