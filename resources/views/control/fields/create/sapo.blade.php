<div class="item form-group @if ($errors->get('sapo')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="sapo">{{__('messages.sapo')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea class="form-control" name="sapo" id="sapo" cols="10" rows="3">{{ old('sapo') }}</textarea>
        @if ($errors->get('sapo'))
          <p class="help is-danger">{{ $errors->first('sapo') }}</p>
        @endif
  </div>
</div>