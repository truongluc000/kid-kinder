<div class="item form-group @if ($errors->get('linkedin')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="linkedin">{{__('messages.linkedin')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="linkedin" class="form-control" name="linkedin" value="{{ old('linkedin') }}" type="text">
    @if ($errors->get('linkedin'))
      <p class="help is-danger">{{ $errors->first('linkedin') }}</p>
    @endif
  </div>
</div>