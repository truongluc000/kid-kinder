<div class="ln_solid">
  <div class="form-group">
    <div class="">
      {{-- col-md-6 col-md-offset-3 --}}
    <input class="btn inlineblock btn-primary" type="submit" value="{{__('messages.save')}}" title="{{__('messages.save')}}" name="btnSubmit">
    <input class="btn inlineblock btn-info" type="reset" value="{{__('messages.clear')}}" title="{{__('messages.clear')}}" name="btnReset">
    <input class="btn inlineblock btn-dark" type="button" value="{{__('messages.cancel')}}" title="{{__('messages.cancel')}}" name="btnCancel" onclick="location.href='{{ url(''.config('constants.adminLink').'/'. Request::segment(2)) }}'">
    </div>
  </div>
</div>