<div class="photo_content">
  <div class="item form-group @if ($errors->get('avatar')) errormsg @endif ">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="avatar">{{__('messages.avatar')}}
    </label>
    <div class="col-md-5 col-sm-6 col-xs-12">
      <input type="text" class="form-control select_photo" value="{{ $item->getProperty('avatar') }}" name="avatar" id="ckfinder-input-1" accept="image/*">
      @if ($errors->get('avatar'))
        <p class="help is-danger">{{ $errors->first('avatar') }}</p>
      @endif
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
      <button type="button" class="btn btn-info" onclick="openPhotoUpload(this);">{{__('messages.select_image')}}</button>
    </div>
  </div>
  @if ($item->getProperty('avatar') ?? '')
  <div class="item form-group display_image">
    <label class="col-form-label col-md-3 col-sm-3 label-align"></label>
    <div class="col-md-3 col-sm-8 col-xs-12">
      <div class="uploadDetails">
          <div class="itemsLeft">
              <div class="typeFile iconImg"><img src="{{ $item->getProperty('avatar') }}"></div>
              <a href="{{ $item->getProperty('avatar') }}" target="_blank" class="nameFile"></a>
          </div>
          <div class="itemsRight">
              <a class="button_in_img" href="javascript:void(0)" onclick="removeImgLink(this)" title="{{__('messages.remove_link')}}"><i class="fa fa-trash"></i> </a>
          </div>
      </div>
    </div>
  </div>
  @endif
</div>