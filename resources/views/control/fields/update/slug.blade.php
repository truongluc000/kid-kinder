<div class="item form-group @if ($errors->get('slug')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="slug">{{__('messages.slug')}} <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="slug" class="form-control" name="slug" value="{{ $item->slug }}" type="text" required="required">
    @if ($errors->get('slug'))
      <p class="help is-danger">{{ $errors->first('slug') }}</p>
    @endif
  </div>
</div>