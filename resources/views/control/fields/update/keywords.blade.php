<div class="item form-group @if ($errors->get('keywords')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="keywords">{{__('messages.keywords')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea class="form-control" name="keywords" id="keywords" cols="10" rows="3">{{ $item->getProperty('keywords') }}</textarea>
        @if ($errors->get('keywords'))
          <p class="help is-danger">{{ $errors->first('keywords') }}</p>
        @endif
  </div>
</div>