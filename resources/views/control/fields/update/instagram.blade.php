<div class="item form-group @if ($errors->get('instagram')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="instagram">{{__('messages.instagram')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="instagram" class="form-control" name="instagram" value="{{ $item->getProperty('instagram') }}" type="text">
    @if ($errors->get('instagram'))
      <p class="help is-danger">{{ $errors->first('instagram') }}</p>
    @endif
  </div>
</div>