<div class="item form-group @if ($errors->get('password')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="password">{{__('messages.password')}}<span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="password" class="form-control" name="password" type="password">
    @if ($errors->get('password'))
      <p class="help is-danger">{{ $errors->first('password') }}</p>
    @endif
  </div>
</div> 