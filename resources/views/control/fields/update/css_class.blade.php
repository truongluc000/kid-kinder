<div class="item form-group @if ($errors->get('css_class')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="css_class">{{__('messages.css_class')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="css_class" class="form-control" name="css_class" value="{{ $item->getProperty('css_class') }}" type="text">
    @if ($errors->get('css_class'))
      <p class="help is-danger">{{ $errors->first('css_class') }}</p>
    @endif
  </div>
</div>