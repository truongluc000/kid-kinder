<div class="photo_content">
  <div class="item form-group @if ($errors->get('api_token')) errormsg @endif ">
    <label class="col-form-label col-md-3 col-sm-3 label-align" for="api_token">{{__('messages.api_token')}}
    </label>
    <div class="col-md-5 col-sm-6 col-xs-12">
      <input type="password" class="form-control api_token password" value="{{ $item->api_token }}" name="api_token">
      @if ($errors->get('api_token'))
        <p class="help is-danger">{{ $errors->first('api_token') }}</p>
      @endif
    </div>
    <div class="col-md-1">
      <button type="button" class="btn btn-info show_input" onclick="showPassword(this);">{{__('messages.show')}}/{{__('messages.hide')}}</button>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12">
      <button type="submit" class="btn btn-warning" >{{__('messages.generate_token')}}</button>
    </div>
  </div>
</div>