<div class="item form-group @if ($errors->get('twitter')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="twitter">{{__('messages.twitter')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="twitter" class="form-control" name="twitter" value="{{ $item->getProperty('twitter') }}" type="text">
    @if ($errors->get('twitter'))
      <p class="help is-danger">{{ $errors->first('twitter') }}</p>
    @endif
  </div>
</div>