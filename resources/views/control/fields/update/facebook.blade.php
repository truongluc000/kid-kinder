<div class="item form-group @if ($errors->get('facebook')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="facebook">{{__('messages.facebook')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="facebook" class="form-control" name="facebook" value="{{ $item->getProperty('facebook') }}" type="text">
    @if ($errors->get('facebook'))
      <p class="help is-danger">{{ $errors->first('facebook') }}</p>
    @endif
  </div>
</div>