<div class="item form-group">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="status">{{__('messages.status')}}<span class="required">*</span>
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
   <select name="status" id="status" class="form-control">
    <option value="1" @if ($item->status == 1) selected @endif >{{ config('constants.status.1') }}</option>
    <option value="0" @if ($item->status == 0) selected @endif>{{ config('constants.status.0') }}</option>
    <option value="2" @if ($item->status == 2) selected @endif>{{ config('constants.status.2') }}</option>
    </select>
  </div>
</div>