<div class="item form-group @if ($errors->get('total')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="total">{{__('messages.total_seats')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="total" class="form-control" name="total" value="{{ $item->getProperty('total') }}" type="text">
    @if ($errors->get('total'))
      <p class="help is-danger">{{ $errors->first('total') }}</p>
    @endif
  </div>
</div>