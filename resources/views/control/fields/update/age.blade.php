<div class="item form-group @if ($errors->get('age')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="age">{{__('messages.age_of_kid')}}
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="age" class="form-control" name="age" value="{{ $item->getProperty('age') }}" type="text">
    @if ($errors->get('age'))
      <p class="help is-danger">{{ $errors->first('age') }}</p>
    @endif
  </div>
</div>