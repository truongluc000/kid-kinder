<div class="item form-group @if ($errors->get('password_confirmation')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="password_confirmation">{{__('messages.password_confirmation')}}<span class="required"></span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="password_confirmation" class="form-control" name="password_confirmation" type="password">
    @if ($errors->get('password_confirmation'))
      <p class="help is-danger">{{ $errors->first('password_confirmation') }}</p> 
    @endif
  </div>
</div>