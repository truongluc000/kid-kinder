<div class="item form-group @if ($errors->get('version_home')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="version_home">{{__('messages.version_home')}}
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
    <input id="version_home" class="form-control" name="version_home" value="{{ $item->getProperty('version_home') }}" type="text">
    @if ($errors->get('version_home'))
      <p class="help is-danger">{{ $errors->first('version_home') }}</p>
    @endif
  </div>
</div>
<div class="item form-group @if ($errors->get('version_admin')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="version_admin">{{__('messages.version_admin')}}
  </label>
  <div class="col-md-2 col-sm-6 col-xs-12">
    <input id="version_admin" class="form-control" name="version_admin" value="{{ $item->getProperty('version_admin') }}" type="text">
    @if ($errors->get('version_admin'))
      <p class="help is-danger">{{ $errors->first('version_admin') }}</p>
    @endif
  </div>
</div>