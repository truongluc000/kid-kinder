<div class="item form-group @if ($errors->get('email')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">{{__('messages.email')}}<span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input id="email" class="form-control" name="email" required="required" value="{{ $item->email }}" type="text">
    @if ($errors->get('email'))
      <p class="help is-danger">{{ $errors->first('email') }}</p>
    @endif
  </div>
</div>