<div class="item form-group @if ($errors->get('opening_days')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="opening_days">{{__('messages.opening_days')}}
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <input id="opening_days" class="form-control" name="opening_days" value="{{ $item->getProperty('opening_days') }}" type="text">
    @if ($errors->get('opening_days'))
      <p class="help is-danger">{{ $errors->first('opening_days') }}</p>
    @endif
  </div>
</div>
<div class="item form-group @if ($errors->get('opening_hours')) errormsg @endif ">
  <label class="col-form-label col-md-3 col-sm-3 label-align" for="opening_hours">{{__('messages.opening_hours')}}
  </label>
  <div class="col-md-3 col-sm-6 col-xs-12">
    <input id="opening_hours" class="form-control" name="opening_hours" value="{{ $item->getProperty('opening_hours') }}" type="text">
    @if ($errors->get('opening_hours'))
      <p class="help is-danger">{{ $errors->first('opening_hours') }}</p>
    @endif
  </div>
</div>