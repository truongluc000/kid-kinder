@if (session('error'))
<div class="errorBox">
  <ul class="alert alert-danger" role="alert" style="list-style: none;">
  <li>{{ session('error') }}</li>
  </ul>
</div>
@endif
@if (session('success'))
<div class="errorBox">
  <ul class="alert alert-success" role="alert" style="list-style: none;">
  <li>{{ session('success') }}</li>
  </ul>
</div>
@endif