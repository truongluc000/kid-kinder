<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{ route('auth.index') }}" class="site_title">
                @if ($configData['avatar'] ?? '')<img src="{{ $configData['avatar'] }}?v=@if ($configData['version_home'] ?? ''){{ $configData['version_home'] }}@endif">@endif 
                <span>@if ($configData['name'] ?? '') {{ $configData['name'] }} @endif</span></a>
            </div>

            <div class="clearfix"></div> 

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="{{ Auth::user()->getProperty('avatar') }}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>{{__('messages.welcome_msg')}},</span>
                <h2>{{ Auth::user()->name }}</h2> 
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                
                <ul class="nav side-menu">
                  <li><a href="{{ route('auth.index') }}"><i class="fa fa-home"></i> {{__('messages.dashboard')}}</a></li>

                  @if (Auth::user()->can('view_menu') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-list"></i> {{__('messages.menus')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('menu.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_menu') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('menu.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_menu') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('menu.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->can('view_classes') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-people-roof"></i> {{__('messages.classes')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('classes.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_classes') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('classes.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_classes') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('classes.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->can('view_teacher') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-graduation-cap"></i> {{__('messages.teachers')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('teacher.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_teacher') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('teacher.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_teacher') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('teacher.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->can('view_gallery') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-image"></i> {{__('messages.gallery')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('gallery.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_gallery') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('gallery.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_gallery') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('gallery.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->can('view_ads_category') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-list-ol"></i> {{__('messages.ads_categories')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('adscategories.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_ads_category') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('adscategories.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_ads_category') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('adscategories.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif
                  @if (Auth::user()->can('view_ads') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-file-image"></i> {{__('messages.ads')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('ads.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_ads') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('ads.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_ads') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('ads.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li>
                  @endif
                  @if (Auth::user()->can('view_article_category') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-list-ul"></i> {{__('messages.article_categories')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('articlecategories.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_article_category') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('articlecategories.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_article_category') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('articlecategories.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif
                  @if (Auth::user()->can('view_article') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-newspaper"></i> {{__('messages.article')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('article.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_article') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('article.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_article') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('article.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif
                  @if (Auth::user()->can('view_comment') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-comments"></i> {{__('messages.comments')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('comment.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('delete_comment') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('comment.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif
                  @if (Auth::user()->can('view_contact') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-phone"></i> {{__('messages.contact')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('contact.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('delete_contact') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('contact.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif 
                  {{-- <li><a><i class="fa fa-comments"></i> {{__('messages.comments')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ url(''.config('constants.adminLink').'/comment') }}">{{__('messages.list_item')}}</a></li>
                      <li><a href="{{ url(''.config('constants.adminLink').'/comment/create') }}">{{__('messages.add_new')}}</a></li>
                    </ul>
                  </li>  --}}
                  @if (Auth::user()->can('view_static') || Auth::user()->hasrole('admin'))
                  <li><a><i class="fa fa-map"></i> {{__('messages.static')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('static.index') }}">{{__('messages.list_item')}}</a></li>
                      @if (Auth::user()->can('add_static') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('static.create') }}">{{__('messages.add_new')}}</a></li>
                      @endif
                      @if (Auth::user()->can('delete_static') || Auth::user()->hasrole('admin'))
                      <li><a href="{{ route('static.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      @endif
                    </ul>
                  </li> 
                  @endif
                  @if (Auth::user()->can('view_library') || Auth::user()->hasrole('admin'))
                  <li><a href="{{ route('library.index') }}"><i class="fa fa-photo-film"></i> {{__('messages.library')}} </a></li>
                  @endif
                </ul>
              </div>

              {{-- Role --}}
              @role('admin') 
              <div class="menu_section">
                <h3>{{__('messages.system')}}</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> {{__('messages.list_user')}} <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('user.index') }}">{{__('messages.list_item')}}</a></li>
                      <li><a href="{{ route('user.create') }}">{{__('messages.add_new')}}</a></li>
                      <li><a href="{{ route('user.clean') }}">{{__('messages.clean_trash')}}</a></li>
                      <li><a href="{{ route('user.trackings') }}">{{__('messages.trackings')}}</a></li>
                    </ul>
                  </li>
                  <li><a href="{{ route('config.show') }}"><i class="fa fa-gear"></i> {{__('messages.config')}} </a></li>
                  
                  {{-- <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li> --}}
                </ul>
              </div>
              @endrole

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('auth.logout') }}">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>