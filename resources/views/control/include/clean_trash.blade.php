@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="mainArticle">
            <div class="clearfix"></div>
          <div class="page-title">
            <div class="title_left">
              <h3>{{__('messages.clean_trash')}}<small></small></h3>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="contType">
                  {!! __('messages.clean_note') !!}
                </div>
                <div class="x_content">
                  @include(''.config('constants.adminLink').'.include.alert')        
                  <form action="{{ url(''.config('constants.adminLink').'/'. Request::segment(2) .'/destroy') }}" method="post" role="form">
                    @method('delete')
                    {{ csrf_field() }}
                    <fieldset>
                      <div class="ln_solid">
                        <div class="form-group">
                          <div class="">
                          <input class="btn inlineblock btn-primary" type="submit" value="{{__('messages.clean_trash')}}" title="{{__('messages.save')}}" name="btnSubmit">
                          <input class="btn inlineblock btn-dark" type="button" value="{{__('messages.cancel')}}" title="{{__('messages.cancel')}}" name="btnCancel" onclick="location.href='{{ url(''.config('constants.adminLink').'/'. Request::segment(2)) }}'">
                          </div>
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
