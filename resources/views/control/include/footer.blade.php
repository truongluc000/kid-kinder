        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Developer by <a href="https://truongluc.com">TruongLuc.com</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
        <!-- jQuery -->
        <script src="{{ url('/') }}/backend/vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ url('/') }}/backend/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <!-- FastClick -->
        <script src="{{ url('/') }}/backend/vendors/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="{{ url('/') }}/backend/vendors/nprogress/nprogress.js"></script>
        <script src="{{ url('/') }}/backend/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <script src="{{ url('/') }}/backend/vendors/iCheck/icheck.min.js"></script>
        <!-- Custom Theme Scripts -->
        <script src="{{ url('/') }}/backend/build/js/custom.min.js"></script>
        <script src="{{ url('/') }}/frontend/js/sweetalert2.js"></script>
        <script src="{{ url('/') }}/backend/build/js/new.js"></script>
      </body>
    </html>