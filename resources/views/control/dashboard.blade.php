@include(''.config('constants.adminLink').'.include.head')
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include(''.config('constants.adminLink').'.include.left')
        @include(''.config('constants.adminLink').'.include.top')
 
        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="mainArticle">
            @role('user')
            <h2>{{__('messages.user_role_note')}}</h2>
            @endrole
            <div class="row dasboardmain">
              @role('admin') 
              <div class="col-lg-2 col-md-55 col-sm-4 col-xs-6">
                <a href="{{ route('user.index') }}" title="{{__('messages.list_user')}}"> 
                      <div class="items one">
                          <div class="flexboxItems">
                            <div class="box-right">
                                  <p class="count">{{ $totalUser }}</p>
                              </div>
                              <div class="icon"><i class="fa fa-user"></i>
                              </div>
                          </div>
                          <div class="title">{{__('messages.list_user')}}</div>
                          <div class="footer-items">
                          </div>
                    </div>
                  </a>
              </div>
              @endrole
              @if (Auth::user()->can('view_classes') || Auth::user()->hasrole('admin'))
              <div class="col-lg-2 col-md-55 col-sm-4 col-xs-6">
                  <a href="{{ route('classes.index') }}" title="{{__('messages.classes')}}"> 
                        <div class="items two">
                            <div class="flexboxItems">
                              <div class="box-right">
                                    <p class="count">{{ $totalClasses }}</p>
                                </div>
                                <div class="icon"><i class="fa fa-people-roof"></i>
                                </div>
                            </div>
                            <div class="title">{{__('messages.classes')}}</div>
                            <div class="footer-items">
                            </div>
                      </div>
                    </a>
              </div>
              @endif
              @if (Auth::user()->can('view_teacher') || Auth::user()->hasrole('admin'))
              <div class="col-lg-2 col-md-55 col-sm-4 col-xs-6">
                  <a href="{{ route('teacher.index') }}" title="{{__('messages.teachers')}}"> 
                        <div class="items three">
                            <div class="flexboxItems">
                              <div class="box-right">
                                    <p class="count">{{ $totalTeacher }}</p>
                                </div>
                                <div class="icon"><i class="fa fa-graduation-cap"></i>
                                </div>
                            </div>
                            <div class="title">{{__('messages.teachers')}}</div>
                            <div class="footer-items">
                            </div>
                      </div>
                    </a>
              </div>
              @endif
              @if (Auth::user()->can('view_gallery') || Auth::user()->hasrole('admin'))
              <div class="col-lg-2 col-md-55 col-sm-4 col-xs-6">
                  <a href="{{ route('gallery.index') }}" title="{{__('messages.gallery')}}"> 
                        <div class="items four">
                            <div class="flexboxItems">
                              <div class="box-right">
                                    <p class="count">{{ $totalGallery }}</p>
                                </div>
                                <div class="icon"><i class="fa fa-image"></i>
                                </div>
                            </div>
                            <div class="title">{{__('messages.gallery')}}</div>
                            <div class="footer-items">
                            </div>
                      </div>
                    </a>
              </div>
              @endif
              @if (Auth::user()->can('view_article') || Auth::user()->hasrole('admin'))
              <div class="col-lg-2 col-md-55 col-sm-4 col-xs-6">
                  <a href="{{ route('article.index') }}" title="{{__('messages.article')}}"> 
                        <div class="items five">
                            <div class="flexboxItems">
                              <div class="box-right">
                                    <p class="count">{{ $totalArticle }}</p>
                                </div>
                                <div class="icon"><i class="fa fa-newspaper"></i>
                                </div>
                            </div>
                            <div class="title">{{__('messages.article')}}</div>
                            <div class="footer-items">
                            </div>
                      </div>
                    </a>
              </div>
              @endif

            </div>
          </div>
        </div>
          <!-- /top tiles -->
          <div class="row">
            <div class="col-md-4 col-sm-4 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Recent Activities <small>Sessions</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="#">Settings 1</a>
                          <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                          </h2>
                            <div class="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                          </h2>
                            <div class="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                          </h2>
                            <div class="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">
                                              <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                          </h2>
                            <div class="byline">
                              <span>13 hours ago</span> by <a>Jane Smith</a>
                            </div>
                            <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                            </p>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-md-8 col-sm-8 ">
              <div class="row">
                <!-- Start to do list -->
                <div class="col-md-6 col-sm-6 ">
                  <div class="x_panel">
                    <div class="x_title">
                      <h2>To Do List <small>Sample tasks</small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                              <a class="dropdown-item" href="#">Settings 1</a>
                              <a class="dropdown-item" href="#">Settings 2</a>
                            </div>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                      </ul>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                      <div class="">
                        <ul class="to_do">
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Schedule meeting with new client </p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Create email address for new intern</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Have IT fix the network printer</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Copy backups to offsite location</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Food truck fixie locavors mcsweeney</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Food truck fixie locavors mcsweeney</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Create email address for new intern</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Have IT fix the network printer</p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> Copy backups to offsite location</p>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End to do list -->
                <div class="col-md-6 col-sm-6">
                    <div class="x_panel tile fixed_height_320">
                      <div class="x_title">
                        <h2>App Versions</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Settings 1</a>
                                <a class="dropdown-item" href="#">Settings 2</a>
                              </div>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <h4>App Usage across versions</h4>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>0.1.5.2</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>123k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
      
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>0.1.5.3</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>53k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>0.1.5.4</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>23k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>0.1.5.5</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>3k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget_summary">
                          <div class="w_left w_25">
                            <span>0.1.5.6</span>
                          </div>
                          <div class="w_center w_55">
                            <div class="progress">
                              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                                <span class="sr-only">60% Complete</span>
                              </div>
                            </div>
                          </div>
                          <div class="w_right w_20">
                            <span>1k</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
      
                      </div>
                    </div>
                  </div>
                  <!--end -->
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        @include(''.config('constants.adminLink').'.include.footer')
