@if ($paginator->hasPages()) 
<div class="col-md-12 mb-4">
    <nav aria-label="Page navigation">
        <ul class="pagination justify-content-center mb-0">
            @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a class="page-link" href="#" aria-label="{{ __('pagination.previous') }}">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">{{ __('pagination.previous') }}</span>
                </a>
            </li>
            @else
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="{{ __('pagination.previous') }}" rel="prev">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">{{ __('pagination.previous') }}</span>
                </a>
            </li>
            @endif

            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                <li class="page-item"><a href="javascript:void(0)" class="disabled page-link">{{ $element }}</a></li>
                @endif
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active"><a href="javascript:void(0)" class="page-link">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a href="{{ $url }}" class="page-link" title="{{ __('Go to page :page', ['page' => $page]) }}">{{ $page }}</a></li>

                        @endif
                    @endforeach
                @endif
            @endforeach
        @if ($paginator->hasMorePages())
        <li class="page-item">
            <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="{{ __('pagination.next') }}">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">{{ __('pagination.next') }}</span>
            </a>
        </li>
        @else
        <li class="page-item disabled">
            <a class="page-link" href="#" aria-label="{{ __('pagination.next') }}">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">{{ __('pagination.next') }}</span>
            </a>
        </li>
        @endif
        </ul>
    </nav>
</div>
@endif
