@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- Team Start -->
    @include('home.teacher') 
    <!-- Team End -->
    <!-- Facilities Start -->
    @include('home.testimonial')  
    <!-- Facilities Start -->
@endsection