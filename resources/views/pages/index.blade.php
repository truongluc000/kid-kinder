@extends('home.app')
@section('content')

    <!-- Header Start -->
    @include('home.slides')  
    <!-- Header End -->
    <!-- Facilities Start -->
    @include('home.facilities') 
    <!-- Facilities Start -->
    <!-- About Start -->
    @include('home.about') 
    <!-- About End -->
    <!-- Class Start -->
    @include('home.class') 
    <!-- Class End --> 
    <!-- Team Start -->
    @include('home.team')
    <!-- Team End -->
    <!-- Testimonial Start -->
    @include('home.testimonial')
    <!-- Testimonial End -->
    <!-- Blog Start -->
    @include('home.blog')
    <!-- Blog End -->

@endsection 
