@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- Gallery Start -->
    @include('home.gallery') 
    <!-- Gallery End -->
@endsection