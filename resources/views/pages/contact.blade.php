@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- Contact detail Start -->
    @include('home.contact') 
    <!-- Contact End -->
@endsection