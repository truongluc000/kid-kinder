@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- Blog detail Start -->
    @include('home.blogdetail') 
    <!-- Blog detail End -->
@endsection