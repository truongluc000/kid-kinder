@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- Novel Start -->
<div class="container-fluid pt-5">
    <div class="container">
        <div class="text-center pb-2">
            <p class="section-title px-5"><span class="px-2">{{__('messages.novel')}}</span></p>
            <h1 class="mb-4">{{__('messages.lastest_novel')}}</h1>
        </div>
        <div class="row pb-3 novel-list"> 
            @if ($novelList ?? '')
            @foreach ($novelList as $itemNovel)
            <div class="col-lg-3 mb-4">
                <div class="card border-0 shadow-sm mb-2">
                    <img class="card-img-top mb-2" src="{{ $itemNovel['avatar'] }}?v={{ $configData['version_home'] }}" alt="">
                    <div class="card-body bg-light text-center p-4">
                        <h4 class="" title="{{ $itemNovel['title'] }}">{{ $itemNovel['title'] }}</h4>
                        <div class="d-flex justify-content-center mb-3">
                            <small class="mr-3"><i class="fa fa-book text-primary"></i> Chương {{ $itemNovel['currentChap'] }} </small>
                            <small class="mr-3"><i class="fa fa-clock text-primary"></i> {{ $itemNovel['lastUpdate'] }} </small>
                        </div>
                        <p class="novel-tags" title="{{ $itemNovel['tags'] }}">{{ $itemNovel['tags'] }}</p>
                        <a href="{{ $itemNovel['url'] }}" class="btn btn-primary px-4 mx-auto my-2" target="_blank">{{__('messages.view_detail')}}</a>
                    </div>
                </div>
            </div>
            @endforeach
            @else

            @endif
        </div>
    </div>
</div>
<!-- Novel End -->
@endsection