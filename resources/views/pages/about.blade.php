@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- About Start -->
    @include('home.about') 
    <!-- About End -->
    <!-- Facilities Start -->
    @include('home.facilities') 
    <!-- Facilities Start -->
    <!-- Team Start -->
    @include('home.team')
    <!-- Team End -->
@endsection 