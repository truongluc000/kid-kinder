@extends('home.app')
@section('content')
    <!-- Header Start -->
    @include('home.header') 
    <!-- Header End -->
    <!-- BLog Start -->
    @include('home.bloglist') 
    <!-- BLog End -->
@endsection