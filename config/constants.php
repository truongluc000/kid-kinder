<?php

return [
    'level' => [
        '1' => 'User',
        '2' => 'Admin',
    ],
    'contact_type' => [
        '0' => 'Contact form',
        '1' => 'Newsletter form',
        '2' => 'Book a seat',
    ], 
    'defaultEmailReceive' => 'truongluc000@gmail.com',
    'newsletter' => 1,
    'bookASeat' => 2,
    'defaultImage' => '/userfiles/images/logo/kid-logo.jpg',
    'articleHomePerPage' => 6,
    'defaultIdConfig' => 1,
    'enable' => '1',// status enable
    'disable' => '0',
    'delete' => '2',
    'defaultLang' => 'en',
    'itemPerPage' => '10',
    'numPageDisplay' => '0',
    'adminLink' => 'control',
    'post' => [
        'limit' => 5
    ],

    'facebook' => [
        'urlFacebook'   => env('FACEBOOK_URL'),
        'clientId'      => env('FACEBOOK_CLIENT_ID'),
        'clientSecret'  => env('FACEBOOK_CLIENT_SECRET'),
    ],

    'status' => [
        '0' => 'Disabled',
        '1' => 'Enabled',
        '2' => 'Deleted',
    ],
    'statusTextBackend' => [
        '0' => '<span class="statusDisabled">Disabled</span>',
        '1' => '<span class="statusEnabled">Enabled</span>',
        '2' => '<span class="statusDeleted">Deleted</span>',
    ],

    'title' => [
        'store'     => 'Add new',
        'update'    => 'Update',
        'list'      => 'List',
    ],
];
